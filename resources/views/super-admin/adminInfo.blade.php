@extends('super-admin')

@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <h4 class="header-title m-t-0 m-b-30">info of {{ $admin->email }}</h4>

                <div class="row">
                    <div class="col-lg-6">

                        <div class="card-box">


                            avatar:
                            @if($admin->avatar != null)
                                <img src="{{asset('avatars/'. $admin->avatar ) }}"
                                     class="img-circle img-thumbnail img-responsive"
                                     style="height: 20%; width: 20%">

                            @else
                                no avatar
                            @endif
                            <br>


                            name:
                            @if($admin->name != null)

                                {{ $admin->name }}
                            @else
                                no name
                            @endif
                            <br>


                            email:
                            @if($admin->email!= null)

                                {{ $admin->email}}<br>
                            @else
                                no email
                            @endif
                            username:
                            @if($admin->username != null)
                                {{ $admin->username }}
                            @else
                                no username
                            @endif
                            <br>

                            company:
                            @if($admin->company != null)
                                {{ $admin->company }}
                            @else
                                no company
                            @endif
                            <br>
                            count of users:
                                {{ $users_count }}
                            <br>


                        </div>

                        List of sub Admins
                        <br>

                        @foreach($admins as $admin )

                            <div class="card-box">
                                <!-- Start content -->
                                {!! link_to_action('AdminController@superAdminDelete', 'delete',[$admin->id]) !!}
                                {{$admin->email}}
                                <br>

                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
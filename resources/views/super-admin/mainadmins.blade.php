@extends('super-admin')

@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <h4 class="header-title m-t-0 m-b-30">list of buyers( company name : )</h4>

                <div class="row">
                    <div class="col-lg-6">
                        @foreach($mains as $main)

                            <div class="card-box">
                                <!-- Start content -->
                                {!! link_to_action('AdminController@mainAdminInfo', $main->company,[$main->id]) !!}
                                <br>

                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="support pannel created for by OmidMohamamd beigi">
    <meta name="author" content="Omid Mohamad Beigi">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title>پنل پشتیبانی | ۲۰۱۸</title>


    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
    {{-- datatables --}}
    <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>


    <!-- X-editable css -->
    <link type="text/css"
          href="{{asset('assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css')}}"
          rel="stylesheet">
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>


    <style>

        @font-face {
            font-family: 'MyWebFont';
            src: url("www.supportpanel.net/panel/fonts/iran-sans/IRANSansWeb_Light.ttf") format('truetype')

        }

        *, html {
            font-family: 'MyWebFont';
        }

    </style>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left"><!-- TODO put company's name here -->
            <a href="index.html" class="logo">

                @if(isset($userav->name) && $userav->name != null )
                    <span> {{$userav->name}}
                        @else
                            <span>خوش امدید</span>
                </span>

                @endif


                <i class="zmdi zmdi-layers"></i></a>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">{{ $name or "" }}</h4>
                    </li>
                </ul>

                <!-- Right(Notification and Searchbox -->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <!-- Notification -->
                        <div class="notification-box">
                            <ul class="list-inline m-b-0">
                                <li>
                                    <a href="javascript:void(0);" class="right-bar-toggle">
                                        <i class="zmdi zmdi-notifications-none"></i>
                                    </a>
                                    <div class="noti-dot">
                                        <span class="dot"></span>
                                        <span class="pulse"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- End Notification bar -->
                    </li>

                    {{--todo active search --}}
                    {{--<li class="hidden-xs">--}}
                    {{--<form role="search" class="app-search">--}}
                    {{--<input type="text" placeholder="جست و جو"--}}
                    {{--class="form-control">--}}
                    {{--<a href=""><i class="fa fa-search"></i></a>--}}
                    {{--</form>--}}
                    {{--</li>--}}
                </ul>

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">
                <div class="user-img">
                    @if(isset($userav) &&  $userav->avatar != null)
                        <img src="{{asset('avatars/'. $userav->avatar) }}"
                             class="img-circle img-thumbnail img-responsive">
                    @else
                        <img src="{{asset('assets/images/users/avatar-1.jpg') }}"
                             class="img-circle img-thumbnail img-responsive">

                    @endif
                    <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
                </div>
                <h5><a href="#">


                        @if(isset($userav->name) && $userav->name != null )
                            {{$userav->name}}
                        @else
                            خوش آمدید
                        @endif

                    </a></h5>
                <ul class="list-inline">
                    <li>
                        <a href="{{ action('UserController@edit') }}">
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    <li>
                        <a href="{{ action('UserController@logout') }}" class="text-custom">
                            <i class="zmdi zmdi-power"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul>


                    <li class="text-muted menu-title">میزکار</li>

                    <li>
                        <a href="{{ action('QuestionController@index') }}" class="waves-effect">
                            <i class="zmdi zmdi-view-dashboard"></i> <span> ارسال تیکت </span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ action('AnswerController@tickets') }}" class="waves-effect">
                            <i class="zmdi zmdi-format-underlined"></i>
                            <span> تیکت ها </span>
                        </a>
                    </li>


                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect">
                            <i class="zmdi zmdi-view-list"></i> <span> پروفایل </span>
                            <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ action('UserController@edit') }}">ویرایش</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"></div>


            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>
    </div>

    <!-- Left Sidebar End -->
@yield('content')





<!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="zmdi zmdi-close-circle-o"></i>
        </a>
        <h4 class="">اطلاعیه ها</h4>
        <div class="notification-list nicescroll">
            <ul class="list-group list-no-border user-list">

                @if(count($ticket_name) == 0)
                    <li class="list-group-item">
                        <a href="#" class="user-list-item">
                            <div class="icon bg-pink">
                                <i class="zmdi zmdi-comment"></i>
                            </div>
                            <div class="user-desc">
                                <span class="name">هیج تیکتی ثبت نشده است</span>
                            </div>
                        </a>
                    </li>
                @endif
                @foreach($ticket_name as $ticket)

                    <li class="list-group-item">
                        <a href="#" class="user-list-item">
                            <div class="icon bg-pink">
                                <i class="zmdi zmdi-comment"></i>
                            </div>
                            <div class="user-desc">
                                <span class="name">{{$ticket->value}}</span>
                                <span class="desc">
                                    @if($ticket->status == null || $ticket->status == 0)

                                        در انتظار پاسخ

                                    @elseif($ticket->status == 1)
                                        پاسخ داده شده
                                    @endif
                                </span>
                                <span class="time"> {{ $ticket->created_at }}</span>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->
{{--<div--}}
{{--style="z-index: 111111111111111">--}}
{{--<iframe src="http://supportpanel.net:8080" height="500px"></iframe>--}}
{{--</div>--}}
{{-- iframe loc--}}

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-rtl.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

<!-- XEditable Plugin -->
<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assets/plugins/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/pages/jquery.xeditable.js')}}"></script>


<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.scroller.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{asset('assets/pages/datatables.init.js')}}"></script>


<script type="text/javascript" src="{{asset('assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>
{{--// fixme in data-token, send a valid token--}}
<script type='text/javascript'
        data-token='{{ $token }}'
        src='http://supportpanel.net/panel/chat.js'></script>

<!-- App js -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax: "{{asset('assets/plugins/datatables/json/scroller-demo.json')}}",
            deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true
        });
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});


        $('form').parsley();

    });
    TableManageButtons.init();

</script>


</body>
</html>
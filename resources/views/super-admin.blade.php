<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="support pannel created  by OmidMohamamd beigi">
    <meta name="author" content="Omid Mohamad Beigi">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title>پنل پشتیبانی | ۲۰۱۸</title>


    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
    {{-- datatables --}}
    <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>


    <!-- Editatable  Css-->
    <link rel="stylesheet" href="{{asset('assets/plugins/magnific-popup/dist/magnific-popup.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/plugins/jquery-datatables-editable/datatables.css')}}"/>

    <!-- X-editable css -->
    <link type="text/css"
          href="{{asset('assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css')}}"
          rel="stylesheet">
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>


    <style>

        @font-face {
            font-family: 'MyWebFont';
            src: url('fonts/iran-sans/IRANSansWeb_Light.ttf') format('truetype')

        }

        *, html {
            font-family: 'MyWebFont';
        }

    </style>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <div id="sidebar-menu">
                <ul>


                    <li class="text-muted menu-title">میزکار</li>
                    <li>
                        <a href="{{ action('AdminController@allMainAdminsSupport') }}" class="waves-effect">
                            <i class="zmdi zmdi-view-dashboard"></i> <span>all admins (support)</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ action('AdminController@allMainAdminsChat') }}" class="waves-effect">
                            <i class="zmdi zmdi-view-dashboard"></i> <span>all admins (chat)</span>
                        </a>
                    </li>

                    {{--<li class="">--}}
                    {{--<a href="#"><i class="fa fa-desktop"></i>رنگ منو<span--}}
                    {{--class="fa arrow"></span></a>--}}
                    {{--<ul class="nav nav-second-level">--}}
                    {{--<br>--}}
                    {{--{!! Form::open(array('action' => 'PanelController@saveColor')) !!}--}}
                    {{--@if(isset($color[0]))--}}
                    {{--<input type="color" name="color" value="{{ $color[0] }}"--}}
                    {{--class="theme" id="bg"--}}
                    {{--style="width: 100%; margin-bottom: 10px">--}}
                    {{--@else--}}
                    {{--<input type="color" name="color" class="theme" id="bg"--}}
                    {{--style="width: 100%; margin-bottom: 10px">--}}
                    {{--@endif--}}
                    {{--<div class="form-group">--}}
                    {{--{!! Form::submit('ذخیره',['class' => 'btn btn-success form-control']) !!}--}}
                    {{--</div>--}}

                    {{--{!! Form::close() !!}--}}

                    {{--</ul>--}}
                    {{--<!-- /.nav-second-level -->--}}
                    {{--</li>--}}


                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->
@yield('content')





<!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="zmdi zmdi-close-circle-o"></i>
        </a>
    </div>
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-rtl.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

<!-- XEditable Plugin -->
<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assets/plugins/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/pages/jquery.xeditable.js')}}"></script>


<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.scroller.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{asset('assets/pages/datatables.init.js')}}"></script>

<!-- Editable js -->
<script src="{{asset('assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/numeric-input-example.js')}}"></script>
<!-- init -->
<script src="assets/pages/datatables.editable.init.js"></script>

<script type="text/javascript" src="{{asset('assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>


<!-- form builder -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src='{{asset('old_assets/js/form-builder.js')}}'></script>
<script src='{{asset('old_assets/js/form-render.js')}}'></script>

<script src="{{asset('old_assets/js/index.js')}}"></script>


<!-- App js -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax: "{{asset('assets/plugins/datatables/json/scroller-demo.json')}}",
            deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true
        });
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});

        $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();

        $('form').parsley();

    });
    TableManageButtons.init();

</script>


</body>
</html>
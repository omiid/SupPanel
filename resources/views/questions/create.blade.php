@extends('main-admin', ['name' => 'ایجاد سوالات'])

@section('content')
    <br>
    <div class="content-page">
        <!-- Start content -->
        {!! csrf_field() !!}
        <meta name="author" content="{{ csrf_token() }}">
        {{ csrf_token() }}
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">ایجاد سوالات</h4>
                            <!-- Start content -->
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <p class="alert alert-success">{{ Session::get('success') }}</p>
                            @endif

                            {!! Form::open(array('action' => ['QuestionController@store'], 'role' => 'form')) !!}
                            {!! Form::label('category', 'دپارتمان : ', ['class' => 'awesome']) !!}
                            {!! Form::select('category_id',$categories,null, ['class' => 'form-control']) !!}
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">



                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-horizontal" role="form">


                                                    <h4 class="header-title m-t-0 m-b-30">ایجاد فرم سوالات </h4>
                                                    <style>
                                                        ul {
                                                            list-style-type: none;
                                                            color: #797979;
                                                        }

                                                        li.list-group-item {
                                                            border-radius: 5px 5px 0 0;
                                                            margin: 0 0 -3px;
                                                            padding: 10px;
                                                            cursor: pointer;
                                                            box-shadow: inset 0 0 0 1px #c5c5c5;
                                                        }
                                                    </style>
                                                    <ul class="list-group">
                                                        {{--<a href="#" class="list-group-item active">--}}
                                                        {{--لورم ایپسوم 1--}}
                                                        {{--</a>--}}
                                                        <li id="text" class="list-group-item"><span>فیلد متنی</span></li>
                                                        <li id="check" class="list-group-item"><span>فیلد چک باکس</span></li>
                                                        <li id="select" class="list-group-item"><span>فیلد کشویی</span></li>
                                                        <li id="radio" class="list-group-item"><span>دکمه رادیویی</span></li>
                                                        <li id="file" class="list-group-item"><span>عکس</span></li>


                                                        {{--<a href="#" class="list-group-item">لورم ایپسوم 2</a>--}}
                                                        {{--<a href="#" class="list-group-item">لورم ایپسوم 3</a>--}}
                                                        {{--<a href="#" class="list-group-item disabled">لورم ایپسوم 4</a>--}}
                                                        {{--<a href="#" class="list-group-item">لورم ایپسوم 5</a>--}}
                                                    </ul> <!-- list-group -->

                                                    <button type="submit"
                                                            class="btn btn-success waves-effect w-md waves-light m-b-5">
                                                        ذخیره
                                                    </button>
                                                    {{--<ul class="rounded">--}}
                                                    {{--<li id="text"><span>فیلد متنی</span></li>--}}
                                                    {{--<li id="check"><span>فیلد چک باکس</span></li>--}}
                                                    {{--<li id="select"><span>فیلد کشویی</span></li>--}}
                                                    {{--<li id="radio"><span>دکمه رادیویی</span></li>--}}
                                                    {{--<li id="file"><span>عکس</span></li>--}}
                                                    {{--</ul>--}}
                                                </div>
                                            </div><!-- end col -->

                                            <div class="col-lg-8">
                                                <div class="form-horizontal" role="form">
                                                    <div id="form" style="
                                                    margin-top: 47px;
                                                    padding: 100px;
                                                    min-height: 200px;
                                                    border: 3px dashed #ccc;
                                                    padding: 10px;"><label id="add-sth" style="position: absolute;

margin: auto;

top: 0;

right: 0;

bottom: 0;

left: 0;

width: 40%;

height: 0;">با کلیک بر روی گزینه های کنار، فرم خود را درست کنید</label>
                                                    </div>
                                                </div>
                                            </div><!-- end col -->

                                        </div><!-- end row -->
                                    </div>
                                </div><!-- end col -->
                            </div>
                            {!! Form::close() !!}


                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                            {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--}}

                            <script>
                                $('#form').on('DOMNodeInserted', function (e) {
                                    $('#add-sth').remove();
                                });

                                $(document).ready(function () {


                                    $('.btnch').click(function () {
                                        alert('asdasd');
                                    });


                                    var counter = 0;
                                    $('#text').click(function () {
                                        $('#form').append(
                                            "<div id='" + counter + "'><br>" +
                                            "<button id='del-" + counter + "' class='btn btn-icon waves-effect waves-light btn-danger btn-sm m-b-1 pull-right'> " +
                                            "<i class='fa fa-remove'></i>" +
                                            " </button>" +
                                            "<label>لطفا متن سوال مورد نظر را در باکس زیر بنویسید</label>" +
                                            "<input type='text' class='form-control' id='txt-" + counter + "' name='question[text][" + counter + "]'>" +

                                            "</div>"
                                        )
                                        ;


                                        $("input[id^='txt-']").each(function () {
                                            var id = parseInt(this.id.replace("txt-", ""), 10);

                                            $("#del-" + id).click(function () {
                                                $('#' + id).remove();
                                                // putBreakHere;
                                            });
                                        });
                                        counter++;
                                    });


                                    //create File
                                    $('#file').click(function () {
                                        $('#form').append(
                                            "<div id='" + counter + "'><br>" +

                                            "<button id='del-" + counter + "' class='btn btn-icon waves-effect waves-light btn-danger btn-sm m-b-1 pull-right'> " +
                                            "<i class='fa fa-remove'></i>" +
                                            " </button>" +
                                            "<label>لطفا سوال مربوط به عکس را در متن زیر بنویسید</label>" +

                                            "<input type='text' id='file-" + counter + "' class='form-control' name='question[file][" + counter + "]'>" +
                                            "</div>");

                                        $("input[id^='file-']").each(function () {
                                            var id = parseInt(this.id.replace("file-", ""), 10);

                                            $("#del-" + id).click(function () {
                                                $('#' + id).remove();
                                                // putBreakHere;
                                            });
                                        });
                                        counter++;
                                    });


                                    // create checkBox OMID
                                    $('#check').click(function (e) {
                                        var count = 0;
                                        // language=HTML
                                        $('#form').append(
                                            "<div id='" + counter + "'><br>" +

                                            "<div class='pull-right'>" +
                                            "<button type='button' id='select_btn-" + counter + "'  class='btn btn-icon waves-effect waves-light btn-primary  btn-sm m-b-1 '> <i class='fa fa-plus'></i> </button>" +
                                            "<button type='button' id='del-" + counter + "' class='btn btn-icon waves-effect waves-light btn-danger btn-sm m-b-1 '> " +
                                            "<i class='fa fa-remove'></i>" +
                                            " </button>" +
                                            "</div>" +

                                            "<label>لطفا سوال و گزینه های چک باکس ها را وارد کنید</label>" +


                                            "<input type='text' class='form-control' name='question[check][" + counter + "]'><br>" +
                                            "<input type='text' placeholder='گزینه ۱'  name='option[" + counter + "][" + count + "]'></div>"
                                        );


                                        $("button[id^='select_btn-']").each(function () {
                                            var id = parseInt(this.id.replace("select_btn-", ""), 10);
                                            var valid = 0;
                                            $("#del-" + id).click(function () {
                                                $('#' + id).remove();
                                                // putBreakHere;
                                            });


                                            $("#select_btn-" + id).click(function () {

                                                count++;
                                                valid = 1;
                                                console.log(valid);
                                                // $(".btnch").click(function () {
                                                var id = parseInt(this.id.replace("select_btn-", ""), 10);
                                                var cnt = count + 1;
                                                $('#' + id).append(
                                                    "<br><input type='text' placeholder='گزینه " + cnt + "' name='option[" + id + "][" + count + "]'>"
                                                    // "<input value='-' type='button' class='form-control' id='del[" + id + "][" + count + "]'><br>"
                                                );
                                                //following line is just an error to stop the loop, I have no idea how to screw the loop :|
                                                putBreakHere;
                                            });
                                        });


                                        counter++;
                                    });


                                    // create radio button 0M!D
                                    $('#radio').click(function () {
                                        var count = 0;

                                        // language=HTML
                                        $('#form').append(
                                            "<div id='" + counter + "'><br>" +


                                            "<div class='pull-right'>" +
                                            "<button type='button' id='select_btn-" + counter + "'  class='btn btn-icon waves-effect waves-light btn-primary  btn-sm m-b-1 '> <i class='fa fa-plus'></i> </button>" +
                                            "<button type='button' id='del-" + counter + "' class='btn btn-icon waves-effect waves-light btn-danger btn-sm m-b-1 '> " +
                                            "<i class='fa fa-remove'></i>" +
                                            " </button>" +
                                            "</div>" +

                                            "<label>لطفا سوال مربوط به دکمه رادیویی و گزینه های مربوطه را در زیر وارد کنید</label>" +


                                            "<input type='text' class='form-control'  name='question[radio][" + counter + "]'><br>" +
                                            "<input type='text'  placeholder='گزینه ۱' name='option[" + counter + "][" + count + "]'></div>"
                                        );


                                        $("button[id^='select_btn-']").each(function () {
                                            var id = parseInt(this.id.replace("select_btn-", ""), 10);
                                            var valid = 0;


                                            $("#del-" + id).click(function () {
                                                $('#' + id).remove();
                                                // putBreakHere;
                                            });

                                            $("#select_btn-" + id).click(function () {

                                                count++;
                                                // $(".btnch").click(function () {
                                                var id = parseInt(this.id.replace("select_btn-", ""), 10);
                                                var cnt = count + 1;
                                                $('#' + id).append(
                                                    "<br><input type='text'  placeholder='گزینه " + cnt + "' name='option[" + id + "][" + count + "]'>"
                                                );
                                                //following line is just an error to stop the loop, I have no idea how to screw the loop :|
                                                putBreakHere;
                                            });
                                        });


                                        counter++;
                                    });

                                    // create select
                                    $('#select').click(function () {
                                        var count = 0;

                                        // language=HTML
                                        $('#form').append(
                                            "<div id='" + counter + "'><br>" +


                                            "<div class='pull-right'>" +
                                            "<button type='button' id='select_btn-" + counter + "'  class='btn btn-icon waves-effect waves-light btn-primary  btn-sm m-b-1 '> <i class='fa fa-plus'></i> </button>" +
                                            "<button type='button' id='del-" + counter + "' class='btn btn-icon waves-effect waves-light btn-danger btn-sm m-b-1 '> " +
                                            "<i class='fa fa-remove'></i>" +
                                            " </button>" +
                                            "</div>" +

                                            "<label>لطفا سوال مربوط به لیست کشویی همراه با گزینه ها را وارد کنید</label>" +


                                            "<input type='text' class='form-control' name='question[select][" + counter + "]'><br>" +
                                            "<input  placeholder='گزینه ۱' type='text' name='option[" + counter + "][" + count + "]'></div>"
                                        );


                                        $("button[id^='select_btn-']").each(function () {
                                            var id = parseInt(this.id.replace("select_btn-", ""), 10);
                                            var valid = 0;

                                            $("#del-" + id).click(function () {
                                                $('#' + id).remove();
                                                // putBreakHere;
                                            });

                                            $("#select_btn-" + id).click(function () {
                                                valid = 1;
                                                count++;
                                                // $(".btnch").click(function () {
                                                var id = parseInt(this.id.replace("select_btn-", ""), 10);
                                                var cnt = count + 1;
                                                $('#' + id).append(
                                                    "<br><input  placeholder='گزینه " + cnt + "' type='text' name='option[" + id + "][" + count + "]'>"
                                                );
                                                //following line is just an error to stop the loop, I have no idea how to screw the loop :|
                                                putBreakHere;
                                            });
                                        });


                                        counter++;
                                    });
                                });


                                // $('.btn-check').click(function () {
                                // });

                            </script>






@stop












{{--<div id="page-wrapper">--}}
{{--<div class="row">--}}
{{--<div class="col-md-8 col-md-offset-2">--}}
{{--<h1 class="page-header">سوالات</h1>--}}
{{--</div>--}}
{{--<!-- /.col-lg-12 -->--}}
{{--</div>--}}
{{--<!-- /.row -->--}}
{{--<div class="row">--}}
{{--<div class="col-md-8 col-md-offset-2">--}}
{{--<div class="panel panel-default">--}}
{{--<div class="panel-heading">--}}
{{--ایجاد سوالات--}}
{{--</div>--}}
{{--<div class="panel-body">--}}
{{--<div class="row">--}}
{{--<div class="col-md-8 col-md-offset-2">--}}
{{--@if (count($errors) > 0)--}}
{{--<div class="alert alert-danger">--}}
{{--<ul>--}}
{{--@foreach ($errors->all() as $error)--}}
{{--<li>{{ $error }}</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--</div>--}}
{{--@endif--}}
{{--{!! Form::open(['action' => 'QuestionController@store', 'class' => 'login']) !!}--}}
{{--<div class="form-group">--}}
{{--{!! Form::label('category', 'دپارتمان : ', ['class' => 'awesome']) !!}--}}
{{--{!! Form::select('category_id',$categories,null, ['class' => 'form-control']) !!}--}}
{{--</div>--}}
{{--<div class="question-body"></div>--}}
{{--<div class="form-group">--}}
{{--{!! Form::label('question', 'question : ', ['class' => 'awesome']) !!}--}}
{{--{!! Form::text('question', null,['placeholder' => 'f name']) !!}--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--{!! Form::label('type', 'type : ', ['class' => 'awesome']) !!}--}}
{{--{!! Form::select('type',[--}}
{{--'-1'=>'plz choose',--}}
{{--'1' =>'text',--}}
{{--'2' => 'drop-down list',--}}
{{--'3' => 'check box' ,--}}
{{--'4' => 'radio button',--}}
{{--'5' => 'image',--}}
{{--],null, ['class' => 'form-control', 'id'=>'type']) !!}--}}


{{--</div>--}}

{{--<div id="option"></div>--}}
{{--{!! Form::button('افزودن سوال',['class' => 'btn addQuestion']) !!}--}}
{{--<br>--}}
{{--<br>--}}
{{--<div class="form-group">--}}
{{--{!! Form::submit('افزودن',['class' => 'btn btn-primary form-control']) !!}--}}
{{--</div>--}}

{{--<script>--}}
{{--var count = 0;--}}
{{--var counter = 0;--}}

{{--$(document).ready(function () {--}}
{{--$(".addQuestion").click(function () {--}}
{{--addQuestion();--}}

{{--});--}}
{{--$(".delQuestion").click(function () {--}}

{{--});--}}
{{--$('.delQuestion').on('click', function () {--}}
{{--//                                            $(this).parent('div.order_number').remove();--}}
{{--});--}}
{{--$(document).on("click", ".delQuestion", function () {--}}
{{--var divId = $('.delQuestion').attr('id');--}}
{{--$(this).parent('div').remove();--}}

{{--//                                            counter--;--}}
{{--//                                            console.log(counter);--}}

{{--});--}}

{{--function addQuestion() {--}}
{{--console.log(counter + "que");--}}

{{--$(".question-body").append("<div class='form-group' id='" + counter + "'>" +--}}
{{--"<label>سوال </label>" +--}}
{{--"<input name='question[" + counter + "]'  class='form-control'  placeholder='  سوال'>" +--}}

{{--"<div class='form-group'>" +--}}
{{--"<label>نوع</label>" +--}}
{{--"<select class='form-control'  name = 'type[" + counter + "]' id='type-" + counter + "'>" +--}}
{{--"<option value='-1'>--لطفا انتخاب کنید--</option>" +--}}
{{--"<option value='1'> متن</option>" +--}}
{{--"<option value='2'> لیست کشویی</option>" +--}}
{{--"<option value='3'> چک باکس</option>" +--}}
{{--"<option value='4'> دکمه رادیو</option>" +--}}
{{--"<option value='5'> عکس</option>" +--}}
{{--"</select>" +--}}

{{--"</div>" +--}}

{{--"<div id='option-" + counter + "'></div>" +--}}
{{--"<button class='btn delQuestion' id='" + counter + "' type='button'>حذف سوال</button></div>");--}}

{{--//                                            $(".question-body").append("");--}}

{{--questionType(counter);--}}

{{--counter++;--}}

{{--}--}}


{{--function questionType(counter) {--}}
{{--$('#type-' + counter).change(function () {--}}
{{--// get the value from select box--}}
{{--var val = $(this).val();--}}
{{--console.log(counter + "ans");--}}


{{--//if val = 1, then make a text box--}}
{{--if (val == 1) {--}}
{{--$("#option-" + counter).empty();--}}

{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='hidden' class='form-control' name='option[" + counter + "][0]'></div><br>"--}}
{{--);--}}
{{--}--}}
{{--// if val = 2, then make a infinitive text box for select options--}}
{{--else if (val == '2') {--}}
{{--$("#option-" + counter).empty();--}}

{{--var count = 0;--}}

{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='button' id='select_btn-" + counter + "' value='+'><br>" +--}}
{{--"<input type='text' class='form-control' name='option[" + counter + "][" + count + "]'><br>"--}}
{{--);--}}
{{--$("#select_btn-" + counter).click(function () {--}}
{{--count++;--}}
{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='text' class='form-control' name='option[" + counter + "][" + count + "]'><br>"--}}
{{--);--}}

{{--});--}}
{{--}--}}
{{--//if val = 3, then make some texts for check box--}}
{{--else if (val == '3') {--}}
{{--$("#option-" + counter).empty();--}}

{{--var count = 0;--}}

{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='button' id='select_btn-" + counter + "' value='+'><br>" +--}}
{{--"<input type='text' class='form-control' name='option[" + counter + "][" + count + "]'><br>"--}}
{{--);--}}
{{--$("#select_btn-" + counter).click(function () {--}}
{{--count++;--}}
{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='text' class='form-control' name='option[" + counter + "][" + count + "]'><br>"--}}
{{--);--}}

{{--});--}}
{{--}--}}

{{--// if val = 4, then make radio button--}}
{{--else if (val == '4') {--}}
{{--$("#option-" + counter).empty();--}}

{{--var count = 0;--}}

{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='button' id='select_btn-" + counter + "' value='+'><br>" +--}}
{{--"<input type='text' class='form-control' name='option[" + counter + "][" + count + "]'><br>"--}}
{{--);--}}
{{--$("#select_btn-" + counter).click(function () {--}}
{{--count++;--}}
{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='text' class='form-control' name='option[" + counter + "][" + count + "]'><br>"--}}
{{--);--}}

{{--});--}}
{{--}--}}
{{--else if (val == '5') {--}}
{{--$("#option-" + counter).empty();--}}

{{--$('div[id=option-' + counter + ']').append(--}}
{{--"<input type='hidden' class='form-control' name='option[" + counter + "][0]'><br>"--}}
{{--);--}}
{{--}--}}

{{--});--}}
{{--}--}}
{{--})--}}
{{--;--}}


{{--</script>--}}


{{--{!! Form::close() !!}--}}


{{--</div>--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@stop--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--}}


@extends('main-admin', ['name' => 'ویرایش سوالات'])

@section('content')



    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">ویرایش اطلاعات</h4>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {!! Form::open(array('action' => ['QuestionController@update', $question->id],  'role' => 'form')) !!}
                            <div class="form-group">
                                {!! Form::label('question', 'سوال :') !!}
                                {!! Form::text('question', $question->question, ['class' => 'form-control']) !!}

                            </div>
                            @if($question->type != 1)

                            <div class="form-group">
                                    @foreach( $options as $option )
                                        <div class="form-group">
                                            {!! Form::text('option['.$option->id.']',
                                             $option->value, ['class' => 'form-control']) !!}
                                        </div>
                                    @endforeach
                            </div>
                            @endif

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">ویرایش
                                </button>
                                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">لغو
                                </button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div><!-- end col -->
                </div>
                @stop


















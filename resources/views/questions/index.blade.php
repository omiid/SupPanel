{{--@extends('app', ['name' => 'دپارتمان ها'] )--}}
{{--@section('content')--}}

    {{--<!-- ============================================================== -->--}}
    {{--<!-- Start right Content here -->--}}
    {{--<!-- ============================================================== -->--}}
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    @foreach($categories as $category)

                        <div class="col-lg-4">
                            <div class="card-box project-box" style=" border: 0;
  box-shadow: 0 8px 10px rgba(0, 0, 0, 0.20), 3px 4px 8px rgba(23, 23, 20, 0.3)">
                                <h4 class="m-t-0 m-b-5">{!! link_to_action('QuestionController@show',
                             $category->name,
                             [$category->id],
                              ['style' => 'color:#5867dd', 'class' =>'text-inverse']) !!}
                                </h4>
                                <p class="text-muted font-13">{{ $category->description }}
                                </p>
                            </div>

                        </div><!-- end col-->
                    @endforeach
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->
        {{--<!-- ============================================================== -->--}}
        {{--<!-- End Right content here -->--}}
        {{--<!-- ============================================================== -->--}}
    </div>

{{--@stop--}}
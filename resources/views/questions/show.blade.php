@extends('app', ['name' => 'ثبت تیکت'] )
@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">ثبت تیکت</h4>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(isset($empty_page))
                                @if($empty_page == '1')
                                    <div class="alert alert-danger">متاسفانه هبچ فیلدی برای پر کردن نیست لطفا با
                                        پشتیبان خود تماس حاصل کنید
                                    </div>
                                @endif
                            @else

                                {!! Form::open(['action' => 'QuestionController@saveShow',
                                 'class' => 'login',
                                 'files' => 'true',
                                 'data-parsley-validate novalidate']) !!}

                                {{--<form action="#" data-parsley-validate novalidate>--}}
                                <div class="form-group">


                                    {!! Form::label('subject',' موضوع ', ['class' => 'awesome']) !!}
                                    {!! Form::text('subject',
                                    null,
                                     ['class' => 'form-control',
                                      'placeholder' => 'موضوع',
                                      'parsley-trigger' => 'change'
                                      ,'required'
                                      ])  !!}
                                </div>
                                <div class="form-group">

                                    {!! Form::label('priority','اولویت') !!}
                                    {!! Form::select('priority',
                                    ['1' => 'بالا', '2' => 'متوسط', '3' => 'کم']
                                    , null,['required','class'=>'form-control select2'])  !!}
                                </div>
                                @foreach($questions as $question)
                                    <h5>{{ $question->question }}</h5>


                                    @if($question->type != 2)
                                        <div class="form-group">
                                            @foreach($options as $option)

                                                @if($question->id == $option->question_id)
                                                    <br>

                                                    @if($question->type == 3)
                                                        {!! Form::checkbox('checkbox['. $option->id .']', $option->id)!!}

                                                        {!! Form::label($option->value,$option->value) !!}
                                                    @endif

                                                    @if($question->type == 4)
                                                        {!! Form::radio('radio[' . $question->id  . ']', $option->id) !!}
                                                        {!! Form::label($option->value,$option->value) !!}
                                                    @endif

                                                    @if($question->type == 1)
                                                        {!! Form::text('text[' . $option->id  . ']',
                                                        null, ['required','class' => 'form-control'])  !!}
                                                    @endif
                                                    @if($question->type == 5)
                                                        {!! Form::file('file[' . $option->id  . ']')  !!}
                                                        <div id="image" data-option-id="{{$option->id}}"></div>
                                                    @endif
                                                @endif

                                            @endforeach
                                                <hr>

                                        </div>
                                    @endif
                                    @if($question->type == 2)
                                        <br>
                                        <select name="select[{{ $question->id }}]" class="form-control">
                                            @foreach($options as $option)
                                                @if($question->id == $option->question_id && $question->type == 2)
                                                    <option value="{{ $option->id }}">{{ $option->value }}</option>
                                                @endif


                                            @endforeach
                                        </select>
                                        <hr>

                                    @endif


                                @endforeach

                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">ارسال
                                    </button>
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">لغو
                                    </button>
                                </div>

                                {!! Form::close() !!}

                            @endif
                        </div>
                    </div><!-- end col -->

                    <!-- ============================================================== -->
                    <!-- End Right content here -->
                    <!-- ============================================================== -->


@stop
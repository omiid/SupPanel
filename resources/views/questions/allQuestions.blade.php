@extends('main-admin', ['name' => 'سوالات مربوطه'])

@section('content')



    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">اقدامات بر روی سوالات</h4>
                            <!-- Start content -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <br>
                @foreach($questions as $question)
                    <div class="alert alert-info">
                        {{$question->question}}

                        <div class="form-group" style="font-size: 150%; float: left;">
                            <a href="{{action('QuestionController@edit',[$question->id])}}">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>

                            <a href="{{action('QuestionController@delete',[$question->id])}}">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>

        </div>
@stop





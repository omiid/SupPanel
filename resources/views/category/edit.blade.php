@extends('main-admin')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">

                <div class="wrap">

                    {!! Form::open(array('action' => ['CategoryController@update', $category->id])) !!}

                    <div class="form-group">
                        {!! Form::label('title', 'عنوان : ', ['class' => 'awesome']) !!}

                        {!! Form::text('update[name]',$category->name,['class' => 'form-control', 'placeholder' => 'عنوان']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('title', 'توضیحات : ', ['class' => 'awesome']) !!}

                        {!! Form::text('update[description]', $category->description,['class' => 'form-control', 'placeholder' => 'توضیحات']) !!}
                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">ذخیره
                        </button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
        </div>
    </div>

@stop

@extends('main-admin', ['name' => 'ایجاد دپارتمان'])
@section('content')


    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">ایجاد دپارتمان</h4>

                            {!! Form::open(array('action' => 'CategoryController@store')) !!}

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(Session::has('success'))
                                <p class="alert alert-success">{{ Session::get('success') }}</p>
                            @endif
                            @if(Session::has('catch'))
                                <p class="alert alert-success">{{ Session::get('catch') }}</p>
                            @endif
                            @if(isset($noCategory))
                                <p class="alert alert-danger">{{ $noCategory }}</p>
                            @endif


                            <div class="form-group">
                                {!! Form::label('title', 'عنوان : ', ['class' => 'awesome']) !!}
                                {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'عنوان']) !!}

                            </div>
                            <div class="form-group">

                                {!! Form::label('description', 'توضیحات: ', ['class' => 'awesome']) !!}
                                {!! Form::text('description', null,['class' => 'form-control', 'placeholder' => 'توضیحات']) !!}
                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">ارسال
                                </button>
                                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">لغو
                                </button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div><!-- end col -->
                </div>


            </div>

        </div>
@stop

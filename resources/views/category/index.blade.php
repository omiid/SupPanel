@extends('main-admin', ['name'=> 'دپارتمان ها'])
@section('content')




    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-4">
                        <a href="{{ action('CategoryController@create') }}"
                           class="btn btn-purple btn-rounded w-md waves-effect waves-light m-b-20">
                            ساخت دپارتمان
                        </a>
                    </div>
                </div>
                <!-- end row -->


                <div class="row">

                    @if(count($categories) <= 0)
                        <div class="alert alert-danger">
                            هیج دپارتمانی موجود نیست. برای ایجاد <a href="{{ action('CategoryController@create') }}">
                                اینجا </a> را کلیک کنید

                        </div>
                    @endif

                    @foreach($categories as $category)

                        <div class="col-lg-4">

                            <div class="card-box project-box" style=" border: 0;
  box-shadow: 0 8px 10px rgba(0, 0, 0, 0.20), 3px 4px 8px rgba(23, 23, 20, 0.3)">
                                <h4 class="m-t-0 m-b-5">
                                    <a href="" class="text-inverse">
                                        {{$category->name}}
                                    </a></h4>

                                <p class="text-muted font-13">{{ $category->description }}
                                </p>

                                <ul class="list-inline">
                                    @if($chat_only != 1)
                                        <li>

                                            {{--<div class="label label-primary">--}}
                                            {!! link_to_action('QuestionController@modifyQuestion', 'سوالات',[$category->id]) !!}
                                            {{--</div>--}}
                                        </li>
                                    @endif
                                    <li>

                                        {{--<div class="label label-pink">--}}
                                        {!! link_to_action('CategoryController@edit', 'ویرایش',[$category->id]) !!}
                                        {{--</div>--}}

                                    </li>
                                    <li>

                                        {{--<div class="label label-danger">--}}

                                        {!! link_to_action('CategoryController@delete', 'حذف ',[$category->id])!!}
                                        {{--</div>--}}
                                    </li>
                                </ul>
                            </div>
                        </div><!-- end col-->
                    @endforeach
                </div>
                <!-- end row -->

            </div> <!-- container -->

        </div> <!-- content -->
@stop

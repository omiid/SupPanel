@extends('app', ['name' => 'تیکت ها'] )
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            {{--<div class="dropdown pull-right">--}}
                                {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"--}}
                                   {{--aria-expanded="false">--}}
                                    {{--<i class="zmdi zmdi-more-vert"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">گزینه 1</a></li>--}}
                                    {{--<li><a href="#">گزینه 2</a></li>--}}
                                    {{--<li><a href="#">گزینه 3</a></li>--}}
                                    {{--<li class="divider"></li>--}}
                                    {{--<li><a href="#">گزینه 4</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}

                            <h4 class="header-title m-t-0 m-b-30">                        تیکت های ارسال شده
                            </h4>

                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>عنوان</th>
                                    <th>وضعیت</th>
                                    <th>اولویت</th>
                                    <th> آخرین به روز رسانی:
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($ticket_name as $ticket)

                                    <tr class="odd gradeX">
                                        <td>{!! link_to_action('AnswerController@userShow', $ticket->value,[$ticket->id]) !!}</td>
                                        <td>@if($ticket->status == null || $ticket->status == 0)

                                                در انتظار پاسخ

                                            @elseif($ticket->status == 1)
                                                پاسخ داده شده
                                            @endif
                                        </td>
                                        <td>
                                            @if($ticket->priority == 1)
                                                بالا
                                            @elseif($ticket->priority == 2)
                                                متوسط
                                            @elseif($ticket->priority == 3)
                                                کم
                                            @endif
                                        </td>
                                        <td class="center"> {{ $ticket->created_at }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>
        </div>
    </div>

@stop

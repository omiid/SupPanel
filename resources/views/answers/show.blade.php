@extends('admin-app', ['name'=>'تیکت'])
@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">تیکت</h4>
                            <!-- Start content -->
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            {!! Form::open(['action'=>'AnswerController@saveReply','class' => 'login']) !!}

                            <br>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    {{ $answer_group[0]->value }}
                                    <br>
                                    <span>
                اولویت :
                                        @if($answer_group[0]->priority == 1)
                                            بالا
                                        @elseif($answer_group[0]->priority == 2)
                                            متوسط
                                        @elseif($answer_group[0]->priority == 3)
                                            کم
                                        @endif</span>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>سوال</th>
                                                <th>پاسخ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($questions as $question)
                                                @foreach($options as  $option)
                                                    <tr>
                                                        @if($question->id == $option->question_id)
                                                            @foreach($answers as $key =>  $answer)
                                                                <input type="hidden"
                                                                       name="groupId[{{ $answer->answer_group_id }}]">
                                                                @if($option->id == $answer->option_id)
                                                                    <td>{{ $key }}</td>

                                                                    <td>{{ $question->question }}</td>
                                                                    @if($question->type == 5)
                                                                        <td>{!! link_to_action('AnswerController@downloadItems', $answer->answer,[$answer->answer]) !!}</td>
                                                                    @endif
                                                                    @if($question->type != 5)
                                                                        @if($answer->answer == null)
                                                                            <td>   {!! Form::label('subject',$option->value, ['class' => 'awesome']) !!}</td>
                                                                        @else
                                                                            <td>    {!! Form::label('subject',$answer->answer, ['class' => 'awesome']) !!}</td>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            @endforeach

                                                        @endif

                                                    </tr>
                                                @endforeach

                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="form-group">
                                            {!! Form::label('reply', 'پاسخ : ', ['class' => 'awesome']) !!}
                                            {!! Form::text('reply', null, ['placeholder' => 'پاسخ', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::submit('ارسال',['class' => 'btn btn-success form-control']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                    </div>
                </div>
@stop







@extends('admin-app', ['name'=> 'تیکت ها'])

@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <h4 class="header-title m-t-0 m-b-30">تیکت ها</h4>

                    @if(count($answer_groups) <= 0)
                        <div class="col-lg-4">
                            <div class="card-box">
                                <!-- Start content -->

                                {{--<div class="alert alert-warning">--}}
                                <div class="panel-heading">

                                    هیچ تیکتی برای پاسخ ندارید
                                </div>
                            </div>
                        </div>
                    @endif

                    @foreach($answer_groups as $answerGroup)

                        <div class="col-lg-4">
                            <div class="card-box">
                                <!-- Start content -->

                                {{--<div class="alert alert-warning">--}}
                                <div class="panel-heading">
                                    {!! link_to_action('AnswerController@show', $answerGroup->value,[$answerGroup->id]) !!}

                                    @if($answerGroup->priority == 1)
                                        <div class="label label-success pull-right">اولویت بالا</div>
                                    @elseif($answerGroup->priority == 2)
                                        <div class="label label-primary pull-right">اولویت متوسط</div>
                                    @else
                                        <div class="label label-pink pull-right">اولویت پایین</div>

                                    @endif

                                </div>
                            {{--</div>--}}

                            <!-- /.col-lg-4 -->
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>


@stop

<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="support pannel created for by OmidMohamamd beigi">
    <meta name="author" content="Omid Mohamad Beigi">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title>پنل پشتیبانی | ۲۰۱۸</title>

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
    {{-- datatables --}}
    <link href="{{asset('assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>


    <!-- Editatable  Css-->
    <link rel="stylesheet" href="{{asset('assets/plugins/magnific-popup/dist/magnific-popup.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/plugins/jquery-datatables-editable/datatables.css')}}"/>

    <!-- X-editable css -->
    <link type="text/css"
          href="{{asset('assets/plugins/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css')}}"
          rel="stylesheet">
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>


    <style>

        @font-face {
            font-family: 'MyWebFont';
            src: url('fonts/iran-sans/IRANSansWeb_Light.ttf') format('truetype')

        }

        *, html {
            font-family: 'MyWebFont';
        }

    </style>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left"><!-- TODO put company's name here -->
            <a href="index.html" class="logo"><span>پنل<span> پشتیبان </span></span><i class="zmdi zmdi-layers"></i></a>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">{{ $name or "" }}</h4>
                    </li>
                </ul>

                <!-- Right(Notification and Searchbox -->
            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">
                <div class="user-img">
                    @if(isset($adminav))
                        @if($adminav->avatar != null)
                            <img src="{{asset('avatars/'. $adminav->avatar) }}"
                                 class="img-circle img-thumbnail img-responsive">
                        @else
                            <img src="{{asset('assets/images/users/avatar-1.jpg') }}"
                                 class="img-circle img-thumbnail img-responsive">
                        @endif
                    @endif

                    <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
                </div>
                {{--<h4>--}}
                <a href="index.html" class="logo"><span>{{ $adminav->name or ""}}</span><i
                            class="zmdi zmdi-layers"></i></a>
                {{--</h4>--}}
                <ul class="list-inline">
                    <li>
                        <a href="{{ action('AdminController@edit') }}">
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    <li>
                        <a href="{{ action('AdminController@logout') }}" class="text-custom">
                            <i class="zmdi zmdi-power"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul>


                    <li class="text-muted menu-title">میزکار</li>

                    @if($chat_only != 1)
                        <li>
                            <a href="{{ action('AnswerController@index') }}" class="waves-effect">
                                <i class="zmdi zmdi-view-dashboard"></i> <span>تیکت ها </span>
                            </a>
                        </li>
                    @endif

                    <li>
                        <a href="{{ action('ChatController@index') }}" class="waves-effect">
                            <i class="zmdi zmdi-view-dashboard"></i> <span>چت انلاین</span>
                        </a>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect">
                            <i class="zmdi zmdi-view-list"></i> <span> پروفایل </span>
                            <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ action('AdminController@edit')}}">ویرایش</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->
@yield('content')





<!-- Right Sidebar -->

    <!-- /Right-bar -->

</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-rtl.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

<!-- XEditable Plugin -->
<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assets/plugins/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/pages/jquery.xeditable.js')}}"></script>


<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.scroller.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{asset('assets/pages/datatables.init.js')}}"></script>

<!-- Editable js -->
<script src="{{asset('assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatables-editable/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
<script src="{{asset('assets/plugins/tiny-editable/numeric-input-example.js')}}"></script>
<!-- init -->
<script src="assets/pages/datatables.editable.init.js"></script>

<script type="text/javascript" src="{{asset('assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>


<!-- form builder -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src='{{asset('old_assets/js/form-builder.js')}}'></script>
<script src='{{asset('old_assets/js/form-render.js')}}'></script>

<script src="{{asset('old_assets/js/index.js')}}"></script>


<!-- App js -->
<script type="text/javascript">
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('#datatable-keytable').DataTable({keys: true});
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax: "{{asset('assets/plugins/datatables/json/scroller-demo.json')}}",
            deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true
        });
        var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});

        $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();

        $('form').parsley();

    });
    TableManageButtons.init();

</script>


</body>
</html>


{{--<!-- Bootstrap Core CSS -->--}}
{{--<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">--}}

{{--<!-- MetisMenu CSS -->--}}
{{--<link href="{{asset('css/plugins/metisMenu/metisMenu.min.css')}}" rel="stylesheet">--}}

{{--<!-- Timeline CSS -->--}}
{{--<link href="{{asset('css/plugins/timeline.css')}}" rel="stylesheet">--}}

{{--<!-- Custom CSS -->--}}
{{--<link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">--}}

{{--<!-- Morris Charts CSS -->--}}
{{--<link href="{{asset('css/plugins/morris.css')}}" rel="stylesheet">--}}

{{--<!-- Custom Fonts -->--}}
{{--<link href="{{asset('css/font-awesome/font-awesome.min.css')}}" rel="stylesheet" type="text/css">--}}

{{--<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--}}
{{--<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
{{--<!--[if lt IE 9]>--}}
{{--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>--}}
{{--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>--}}
{{--<![endif]-->--}}

{{--</head>--}}

{{--<body>--}}

{{--<div id="wrapper" style="background-color: rgb(37, 41, 50);">--}}

{{--<!-- Navigation -->--}}
{{--<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #5867dd">--}}
{{--<div class="navbar-header">--}}
{{--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">--}}
{{--<span class="sr-only">Toggle navigation</span>--}}
{{--<span class="icon-bar"></span>--}}
{{--<span class="icon-bar"></span>--}}
{{--<span class="icon-bar"></span>--}}
{{--</button>--}}
{{--<a class="navbar-brand" href="">Turbo VPN</a>--}}
{{--</div>--}}
{{--<!-- /.navbar-header -->--}}

{{--<ul class="nav navbar-top-links navbar-right">--}}

{{--<!-- /.dropdown -->--}}
{{--<li class="dropdown">--}}
{{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">--}}
{{--<i class="fa fa-user fa-fw" style="color: #fff;"></i> <i class="fa fa-caret-down"></i>--}}
{{--</a>--}}
{{--<ul class="dropdown-menu dropdown-user">--}}
{{--<li><a href="{{ action('AdminController@edit') }}"><i class="fa fa-gear fa-fw"></i> تنظیمات</a>--}}
{{--</li>--}}
{{--<li class="divider"></li>--}}
{{--<li><a href="{{ action('AdminController@logout') }}"><i class="fa fa-sign-out fa-fw"></i> خروج</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--<!-- /.dropdown-user -->--}}
{{--</li>--}}
{{--<!-- /.dropdown -->--}}
{{--</ul>--}}
{{--<!-- /.navbar-top-links -->--}}

{{--<div class="navbar-default sidebar" role="navigation">--}}
{{--<div class="sidebar-nav navbar-collapse">--}}
{{--<ul class="nav" id="side-menu" style="background-color: rgb(37, 41, 50);">--}}

{{--<li>--}}
{{--<a class="active" href="{{ action('AnswerController@index') }}"><i class="fa fa-dashboard fa-fw"></i> داشبورد</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a class="active" href="{{ action('ChatController@index') }}"><i class="fa fa-envelope" aria-hidden="true"></i> چت آنلاین </a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#"><i class="fa fa-user fa-fw"></i>پروفایل<span class="fa arrow"></span></a>--}}
{{--<ul class="nav nav-second-level">--}}
{{--<li>--}}
{{--<a href="{{ action('AdminController@edit') }}">ویرایش </a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--<!-- /.nav-second-level -->--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<!-- /.sidebar-collapse -->--}}
{{--</div>--}}
{{--<!-- /.navbar-static-side -->--}}
{{--</nav>--}}


{{--@yield('content')--}}


{{--</div>--}}

{{--<!-- jQuery Version 1.11.0 -->--}}
{{--<script src="{{asset('js/jquery-1.11.0.js')}}"></script>--}}

{{--<!-- Bootstrap Core JavaScript -->--}}
{{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}

{{--<!-- Metis Menu Plugin JavaScript -->--}}
{{--<script src="{{asset('js/metisMenu/metisMenu.min.js')}}"></script>--}}

{{--<!-- Morris Charts JavaScript -->--}}
{{--<script src="{{asset('js/raphael/raphael.min.js')}}"></script>--}}
{{--<script src="{{asset('js/morris/morris.min.js')}}"></script>--}}

{{--<!-- Custom Theme JavaScript -->--}}
{{--<script src="{{asset('js/sb-admin-2.js')}}"></script>--}}
{{--<script src="{{asset('js/jquery/jquery.dataTables.min.js')}}"></script>--}}
{{--<script src="{{asset('js/bootstrap/dataTables.bootstrap.min.js')}}"></script>--}}
{{--<script>--}}
{{--$(document).ready(function() {--}}
{{--$('#dataTables-example').dataTable();--}}
{{--});--}}
{{--</script>--}}

{{--</body>--}}

{{--</html>--}}


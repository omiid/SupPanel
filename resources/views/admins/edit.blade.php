@extends(($main_admin ? 'main-admin' : 'admin-app'), ['name'=>'ویرایش'])
@section('content')



    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">ویرایش اطلاعات</h4>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('edited'))
                                <p class="alert alert-success">{{ Session::get('edited') }}</p>
                            @endif
                            @if(Session::has('image_error'))
                                <p class="alert alert-danger">{{ Session::get('image_error') }}</p>
                            @endif
                            @if(Session::has('catch'))
                                <p class="alert alert-danger">{{ Session::get('catch') }}</p>
                            @endif

                            {!! Form::open(array('action' => ['AdminController@update'],
                             'role' => 'form',
                             'files' => 'true')) !!}
                            <div class="form-group">
                                {!! Form::label('name', 'نام و نام خانوادگی ', ['class' => 'awesome']) !!}
                                {!! Form::text('admin[name]',
                                 $admin->name,
                                 ['class' => 'form-control',
                                 'parsley-trigger' => 'change',
                                  'placeholder' => 'نام',
                                  'required'
                                  ]) !!}

                            </div>
                            <div class="form-group">
                                {!! Form::label('email', 'ایمیل * ', ['class' => 'awesome']) !!}
                                {!! Form::text('admin[email]',
                                $admin->email,['class' => 'form-control',
                                 'placeholder' => 'ایمیل',
                                 'parsley-trigger' => 'change',
                                 'required']) !!}

                            </div>
                            {!! Form::file('file')  !!}

                            @if(($main_admin ? 1 : 0) == 1)
                                <div class="form-group">
                                    {!! Form::label('company', 'شرکت * ', ['class' => 'awesome']) !!}
                                    {!! Form::text('admin[company]',
                                    $admin->company,['class' => 'form-control',
                                     'placeholder' => 'نام شرکت',
                                     'parsley-trigger' => 'change',
                                     'required']) !!}

                                </div>
                            @endif
                            {!! Form::label('password', 'رمز عبور : ', ['class' => 'awesome']) !!}
                            {!! Form::text('admin[password]',
                             $admin->password,
                             ['class' => 'form-control', 'placeholder' => 'رمز عبور'
                             ,'id' => 'pass1']) !!}

                            <br>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">ارسال
                                </input>
                                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">لغو
                                </button>
                            </div>

                            {!! Form::close() !!}


                        </div>
                    </div><!-- end col -->
                </div>
@stop
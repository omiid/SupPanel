@extends('main-admin', ['name'=> 'لینک ها'])
@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <!-- Start content -->
                            <h4 class="header-title m-t-0 m-b-30">طریقه استفاده</h4>

                            <p class="text-muted m-b-15 font-13">
                                لینک زیر را به ادمین ها داده،‌یا در وب سایت خود داخل تگ <code> &lt;a&gt; </code>بصورت
                                زیر قرار دهید
                            </p>

                            {{--<p class="text-muted m-b-15 font-13">--}}
                                {{--<code> &lt;a href="{!! link_to($admin_login, $admin_login) !!}"--}}
                                    {{--&gt; {{'لینک ورودی ادمین ها'}}--}}
                                    {{--&lt;/a&gt;</code>--}}
                            {{--</p>--}}
                            <div class="alert alert-info">
                                <br>
                                {!! link_to($admin_login, $admin_login) !!}
                            </div>
                            @if($chat_only != 1)

                                <p class="text-muted m-b-15 font-13">
                                    لینک زیر را در وب سایت خود داخل تگ <code> &lt;a&gt; </code> قرار دهید
                                </p>
                                <div class="alert alert-info">
                                    {{'لینک ورودی کاربران ها'}}
                                    <br>
                                    {!! link_to($user_login, $user_login) !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

@stop
@extends('main-admin')
@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    @if(count($admins) <= 0)
                        <div class="alert alert-danger">
                            هیج پشتیبانی موجود نیست. برای ایجاد <a href="{{ action('AdminController@create') }}">
                                اینجا </a>
                            را کلیک کنید

                        </div>
                    @endif
                    @foreach($admins as $admin)
                        <div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">

                            <div class="alert alert-info">
                                {{ $admin->email }}
                                <div class="label label-danger pull-right">
                                    {!! link_to_action('AdminController@delete', 'حذف',[$admin->id]) !!}
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>

@stop
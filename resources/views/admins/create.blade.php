@extends('main-admin', ['name'=> 'افزودن پشتیبان'])
@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-8">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">افزودن پشتیبان</h4>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                @if(Session::has('stored'))
                                    <p class="alert alert-success">{{ Session::get('stored') }}</p>
                                @endif
                                @if(Session::has('wrong-reg'))
                                    <p class="alert alert-danger">{{ Session::get('wrong-reg') }}</p>
                                @endif
                                @if(Session::has('user or mail exists'))
                                    <p class="alert alert-danger">{{ Session::get('user or mail exists') }}</p>
                                @endif
                                {!! Form::open(array('action' => ['AdminController@store'], 'role' => 'form')) !!}

                                {{--<div class="form-group">--}}
                                {{--{!! Form::label('last_name', 'نام نام خانوادگی :', ['class' => 'awesome']) !!}--}}
                                {{--{!! Form::text('admin[last_name]', null,['placeholder' => 'نام خانوادگی ', 'class' => 'form-control']) !!}--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    {!! Form::label('username', 'نام کاربری', ['class' => 'awesome']) !!}
                                    {!! Form::text('username', null,['placeholder' => 'نام کاربری', 'class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('email', 'آدرس ایمیل : ', ['class' => 'awesome']) !!}
                                    {!! Form::text('email', null,['placeholder' => 'ایمیل', 'class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('password', 'رمز عبور : ', ['class' => 'awesome']) !!}
                                    {!! Form::password('password', ['placeholder' => 'رمز عبور', 'class' => 'form-control']) !!}
                                </div>
                                    <div class="form-group">
                                        {!! Form::label('category', 'دپارتمان  : ', ['class' => 'awesome']) !!}
                                        {!! Form::select('category[]',$categories,null, ['class' => 'form-control cat', 'multiple']) !!}
                                    </div>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">ارسال
                                </button>
                                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">لغو
                                </button>
                            </div>


                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop





    {{--<!DOCTYPE html>--}}
    {{--<html lang="en">--}}

    {{--<head>--}}

    {{--<meta charset="utf-8">--}}
    {{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    {{--<meta name="description" content="">--}}
    {{--<meta name="author" content="">--}}

    {{--<title>پشتیبانی</title>--}}

    {{--<!-- Bootstrap Core CSS -->--}}
    {{--<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">--}}

    {{--<!-- MetisMenu CSS -->--}}
    {{--<link href="{{asset('css/plugins/metisMenu/metisMenu.min.css')}}" rel="stylesheet">--}}

    {{--<!-- Timeline CSS -->--}}
    {{--<link href="{{asset('css/plugins/timeline.css')}}" rel="stylesheet">--}}

    {{--<!-- Custom CSS -->--}}
    {{--<link href="{{asset('css/sb-admin-2.css')}}" rel="stylesheet">--}}

    {{--<!-- Morris Charts CSS -->--}}
    {{--<link href="{{asset('css/plugins/morris.css')}}" rel="stylesheet">--}}

    {{--<!-- Custom Fonts -->--}}
    {{--<link href="{{asset('css/font-awesome/font-awesome.min.css')}}" rel="stylesheet" type="text/css">--}}

    {{--<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--}}
    {{--<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
    {{--<!--[if lt IE 9]>--}}
    {{--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>--}}
    {{--<![endif]-->--}}

    {{--</head>--}}

    {{--<body>--}}

    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--<div class="col-md-4 col-md-offset-4">--}}
    {{--<div class="login-panel panel panel-default">--}}
    {{--<div class="panel-heading">--}}
    {{--<h3 class="panel-title">--}}
    {{--ساخت حساب کاربری--}}
    {{--</h3>--}}
    {{--</div>--}}
    {{--<div class="panel-body">--}}
    {{--@if (count($errors) > 0)--}}
    {{--<div class="alert alert-danger">--}}
    {{--<ul>--}}
    {{--@foreach ($errors->all() as $error)--}}
    {{--<li>{{ $error }}</li>--}}
    {{--@endforeach--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--@if(Session::has('stored'))--}}
    {{--<p class="alert alert-success">{{ Session::get('stored') }}</p>--}}
    {{--@endif--}}
    {{--@if(Session::has('wrong-reg'))--}}
    {{--<p class="alert alert-danger">{{ Session::get('wrong-reg') }}</p>--}}
    {{--@endif--}}
    {{--@if(Session::has('user or mail exists'))--}}
    {{--<p class="alert alert-danger">{{ Session::get('user or mail exists') }}</p>--}}
    {{--@endif--}}
    {{--{!! Form::open(array('action' => ['AdminController@store'], 'role' => 'form')) !!}--}}

    {{--<div class="form-group">--}}
    {{--{!! Form::label('last_name', 'نام نام خانوادگی :', ['class' => 'awesome']) !!}--}}
    {{--{!! Form::text('admin[last_name]', null,['placeholder' => 'نام خانوادگی ', 'class' => 'form-control']) !!}--}}
    {{--</div>--}}
    {{--<div class="form-group">--}}
    {{--{!! Form::label('username', 'نام کاربری', ['class' => 'awesome']) !!}--}}
    {{--{!! Form::text('username', null,['placeholder' => 'نام کاربری', 'class' => 'form-control']) !!}--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--{!! Form::label('email', 'آدرس ایمیل : ', ['class' => 'awesome']) !!}--}}
    {{--{!! Form::text('email', null,['placeholder' => 'ایمیل', 'class' => 'form-control']) !!}--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--{!! Form::label('password', 'رمز عبور : ', ['class' => 'awesome']) !!}--}}
    {{--{!! Form::password('password', ['placeholder' => 'رمز عبور', 'class' => 'form-control']) !!}--}}
    {{--</div>--}}
    {{--@if($main_admin == 1)--}}
    {{--<div class="form-group">--}}
    {{--{!! Form::label('category', 'دپارتمان  : ', ['class' => 'awesome']) !!}--}}
    {{--{!! Form::select('category[]',$categories,null, ['class' => 'form-control cat', 'multiple']) !!}--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--<div class="form-group">--}}
    {{--{!! Form::submit('ثبت نام',['class' => 'btn btn-primary form-control']) !!}--}}
    {{--</div>--}}

    {{--{!! Form::close() !!}--}}
    {{--@if($main_admin == 1)--}}
    {{--<div class="form-group">--}}
    {{--<a href="{{ action('AdminController@index') }}"--}}
    {{--class="btn btn-primary form-control">--}}
    {{--بازگشت به صفخه ی اصلی--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<script>--}}
    {{--$(document).ready(function() {--}}
    {{--var categoryValue = $(".cat").val();--}}
    {{--if(!categoryValue )--}}
    {{--alert('لطفا ابتدا دپارتمان را تعریف کنید‌!!');--}}
    {{--});--}}
    {{--</script>--}}
    {{--<!-- jQuery Version 1.11.0 -->--}}
    {{--<script src="{{asset('js/jquery-1.11.0.js')}}"></script>--}}

    {{--<!-- Bootstrap Core JavaScript -->--}}
    {{--<script src="{{asset('js/bootstrap.min.js')}}"></script>--}}

    {{--<!-- Metis Menu Plugin JavaScript -->--}}
    {{--<script src="{{asset('js/metisMenu/metisMenu.min.js')}}"></script>--}}

    {{--<!-- Custom Theme JavaScript -->--}}
    {{--<script src="{{asset('js/sb-admin-2.js')}}"></script>--}}

    {{--</body>--}}

    {{--</html>--}}












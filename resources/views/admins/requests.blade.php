@extends('admin-app')
@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="test alert alert-warning" style="border: none"></div>
        </div>
    </div>
@stop
{{-- if id = 0 then admin hasn't open the chat yet
     if id = 1 then admin id chatting
     if id = 2 then admin or user left the chat
 --}}
<script>

    var id = [];

    function getUserRequest() {
        var jsonString;

        if (id.length == 0)
            id.push(0);
        jsonString = JSON.stringify(id);

        $.ajax({
            type: "GET",
            url: "{{url('getUserRequest')}}",
            data: {data: jsonString},

            success: function (res) {
                if (res) {
                    $(".test").append(res);

                    $.each(res, function (data, val) {
                        var status = val.status;
                        if (status == 0) {
                            id.push(val.id);
                            $(".test").append('' +
                                '<div style="' +
                                'top:0%;' +
                                'margin-left:2%;' +
                                'margin-bottom:2%;' +
                                'width:inherit;' +
                                'height:470px;' +
                                'display:inline-block;' +
                                'float:left;"' +
                                'class=" ' + val.id + ' "' +
                                '>' +

                                '<iframe src="http://www.' + val.link + '" style="height: 470px; width: 300px;"></iframe>' +
                                '</div>'

//                           '<a href="' + val.link + '">' + val.link + '</a>'
                            );
//                        $(".test").append('<br>');

                        }


                    });
                } else {
                    $(".test").append('not done');
                }
            }
        });
    }

    var arr = [];

    function endStatus() {
        var jsonString;

        if (arr.length == 0)
            arr.push(0);
        jsonString = JSON.stringify(arr);

        $.ajax({
            type: "GET",
            url: "{{url('getUserRequest')}}",
            data: {data: jsonString},

            success: function (res) {
                if (res) {
                    $(".test").append(res);

                    $.each(res, function (data, val) {
                        var status = val.status;
                        if (status == 2) {
                            arr.push(val.id);
                            console.log('this status is 2');
                            if ($('div').hasClass(val.id)) {
                                $('div').remove('.' + val.id);
                                console.log('new one has been deleted');
                            }

                        }


                    });
                }
                else {
                    $(".test").append('not done');
                }
            }
        });
    }


    setInterval(endStatus, 6000);
    setInterval(getUserRequest, 6000);
</script>
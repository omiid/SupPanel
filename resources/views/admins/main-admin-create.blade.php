<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="support pannel created for by OmidMohamamd beigi">
    <meta name="author" content="Omid Mohamad Beigi">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- App title -->
    <title>پنل پشتیبانی | ۲۰۱۸</title>


    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap-rtl.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

</head>
<body>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="index.html" class="logo"><span> پشتیبان <span>سیستم</span></span></a>
        <h5 class="text-muted m-t-0 font-600">به مشتریان خود حس بهتری دهید</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">ثبت نام</h4>
        </div>
        <div class="panel-body">

            {{-- to modify validations message, use this link https://codepen.io/pen/--}}
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('stored'))
                <p class="alert alert-success">{{ Session::get('stored') }}</p>
            @endif
            @if(Session::has('wrong-reg'))
                <p class="alert alert-danger">{{ Session::get('wrong-reg') }}</p>
            @endif
            @if(Session::has('user or mail exists'))
                <p class="alert alert-danger">{{ Session::get('user or mail exists') }}</p>
            @endif

            @if(isset($chat_only))
                @if($chat_only == 1)
                    {!! Form::open(array('action' => ['AdminController@storeOnlyChat'],
                     'role' => 'form', 'class' => 'form-horizontal m-t-20')) !!}
                @else

                    {!! Form::open(array('action' => ['AdminController@store'],
                         'role' => 'form', 'class' => 'form-horizontal m-t-20')) !!}
                @endif
            @endif
            @if(!isset($chat_only))
                {!! Form::open(array('action' => ['AdminController@store'],
                 'role' => 'form', 'class' => 'form-horizontal m-t-20')) !!}
            @endif

            <form class="form-horizontal m-t-20" action="index.html">

                <div class="form-group ">
                    <div class="col-xs-12">
                        {!! Form::input('email', 'email',
                        null,['placeholder' => 'پست الکترونیکی',
                        'class' => 'form-control',
                        'autofocus']) !!}
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        {!! Form::text('username',
                        null,
                        ['placeholder' => ' نام کاربری',
                         'class' => 'form-control',
                         'required']) !!}
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        {!! Form::text('company',
                         null,['placeholder' => 'شرکت',
                         'class' => 'form-control', 'required']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">

                        <input class="form-control"
                               type="password"
                               name="password"
                               required=""
                               placeholder="رمز عبور">
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light"
                                type="submit">ثبت
                            نام
                        </button>
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>
    <!-- end card-box -->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">حساب کاربری دارید؟<a href="{{ url('/admin/login') }}" class="text-primary m-l-5"><b>وارد
                        شوید</b></a></p>
        </div>
    </div>

</div>
<!-- end wrapper page -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap-rtl.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

</body>
</html>

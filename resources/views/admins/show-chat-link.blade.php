@extends('main-admin', ['name'=> 'چت باکس'])
@section('content')

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-8">
                        <div class="card-box">
                            @if(Session::has('catch'))
                                <p class="alert alert-danger">{{ Session::get('catch') }}</p>
                            @endif


                            <h4 class="header-title m-t-0 m-b-30">یک چت زنده در وب سایت خود بسازید</h4>

                            <p class="text-muted m-b-15 font-13">
                                کد <code>script </code>زیر را در پایین وب سایت خود بعد از کتابخانه jquery اضافه نمایید. بدین منظور ابتدا
                                پشتیبان خود را ساخته تا با توجه به دپارتمان تعریف شده بتوانند پاسخگو چت باشند
                            </p>

                            <pre>
                                        &lt;script type='text/javascript'
                                                data-token='{{$token}}'
                                                src='http://{{$sub}}.supportpanel.net/panel/chat.js'&gt; &lt;/script&gt;
                                        </pre>

                            <!-- Remove (Demo only) -->
                            <div class="visible-lg" style="height: 92px;"></div>

                        </div>
                    </div><!-- end col -->
                    {{--{!! Form::label('chat', "", ['class' => 'awesome']) !!}--}}


                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop













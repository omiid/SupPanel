
@if(Session::has('catch'))
    <p class="alert alert-danger">{{ Session::get('catch') }}</p>
@endif
<div id="flip" style="
position: fixed;
border: 0;
z-index: 2147483642;
bottom: 0%;
width: 300px;
text-align: center;
background-color: rgb(0, 150, 136);
color: rgb(240, 241, 241);
padding: 0.5%;
border: 1px solid #1c92c7;
margin-right: 1%;
border-top-right-radius: 30px;
cursor: pointer;

">

    <b>
        همراه شما هستیم - گفتگو آنلاین
    </b>
</div>

<div id="chatFrame" style="
display: none;
position: fixed;
border: 0;
z-index: 2147483642;
bottom: 0%;
width: 350px;
height: 470px;
">

    <div id="button" style="
cursor: pointer;
width: 23px !important;
height: 23px !important;
position: relative !important;
top: 0 !important;
left: 0 !important;
direction: ltr !important;
margin-bottom: 1px;
float: left;
display: block !important;
opacity: 1;
visibility: visible !important;">
        <svg id="jivo-icon-closewidget" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <style>.jivo-st0 {
                    opacity: .8;
                    fill: #FFF;
                    stroke: #000;
                    enable-background: new
                }

                .jivo-st0, .jivo-st1 {
                    stroke-width: 1.5;
                    stroke-linecap: round
                }

                .jivo-st1 {
                    fill: none;
                    stroke: #383d45
                }</style>
            <circle class="jivo-st0" cx="12" cy="12" r="11"></circle>
            <path class="jivo-st1" d="M7.5 16.5l9-9M16.5 16.5l-9-9"></path>
        </svg>
    </div>
    <iframe src="http://supportpanel.net:8080/{{ $token }}" id="iframe"
            style="border-radius: 10px; height: 100%; width: 320px;"></iframe>

</div>

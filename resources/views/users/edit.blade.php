@extends('app', ['name' => 'ویرایش'] )

@section('content')



    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">ویرایش اطلاعات</h4>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('edited'))
                                <p class="alert alert-success">{{ Session::get('edited') }}</p>
                            @endif
                            {!! Form::open(array('action' => ['UserController@update', $user->id],
                             'role' => 'form', 'data-parsley-validate novalidate',
                             'files' => 'true')) !!}
                            <div class="form-group">
                                {!! Form::label('username', 'نام کاربری : ', ['class' => 'awesome']) !!}
                                {!! Form::text('user[username]',
                                 $user->username,
                                 ['class' => 'form-control',
                                 'parsley-trigger' => 'change',
                                  'placeholder' => 'نام کاربری',
                                  'required']) !!}

                            </div>
                            <div class="form-group">
                                {!! Form::label('name', 'نام و نام خانوادگی ', ['class' => 'awesome']) !!}
                                {!! Form::text('user[name]',
                                 $user->name,
                                 ['class' => 'form-control',
                                 'parsley-trigger' => 'change',
                                  'placeholder' => 'نام',
                                  'required']) !!}

                            </div>
                            <div class="form-group">
                                {!! Form::label('email', 'ایمیل * ', ['class' => 'awesome']) !!}
                                {!! Form::text('user[email]',
                                $user->email,['class' => 'form-control',
                                 'placeholder' => 'ایمیل',
                                 'parsley-trigger' => 'change',
                                 'required']) !!}
                                {!! Form::file('file')  !!}

                            </div>
                            {!! Form::label('password', 'رمز عبور : ', ['class' => 'awesome']) !!}
                            {!! Form::text('user[password]',
                             $user->password,
                             ['class' => 'form-control', 'placeholder' => 'رمز عبور'
                             ,'id' => 'pass1']) !!}

                            <div class="form-group">
                                <label for="passWord2">رمز عبور *</label>
                                <input data-parsley-equalto="#pass1" type="password" required
                                       placeholder="رمز عبور" class="form-control" id="passWord2">
                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">ارسال
                                </button>
                                <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">لغو
                                </button>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div><!-- end col -->
                </div>
@stop

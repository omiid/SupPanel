jQuery(function($) {
  var $fbEditor = $(document.getElementById('fb-editor')),
    $formContainer = $(document.getElementById('fb-rendered-form')),
    fbOptions = {
        disableFields: [
            'autocomplete',
            'button',
            'header',
            'hidden',
            'date',
            'number',
            'textarea',
            'paragraph'
        ],
        disabledActionButtons: ['data'],
        disabledAttrs: ["value",
            "placeholder",
            "required",
            "description",
            "toggle",
            "inline",
            "className",
            "subtype",
            "multiple",
            "maxlength",
            "name",
            "access",
            "other"



        ],
        onSave: function() {
        $fbEditor.toggle();
        $formContainer.toggle();
        $('form', $formContainer).formRender({
          formData: formBuilder.formData
        });
      }
    },
    formBuilder = $fbEditor.formBuilder(fbOptions);

  $('.edit-form', $formContainer).click(function() {
    $fbEditor.toggle();
    $formContainer.toggle();
  });
});


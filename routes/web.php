<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * chat test
 */
Route::post('sendmessage', 'chatController@sendMessage');
/**
 * end
 */

/**
 * test
 */
//Route::get('/', 'AdminController@url');
//Route::get('test', function(){
//    return view('test');
//});
//Route::get('test', 'AdminController@testVie');
//Route::post('test', 'AdminController@test');
//Route::get('omid', function () {
//    return view('welcome');
//});
Route::post('saveColor', 'PanelController@saveColor');
/**
 * test end
 */


/**
 *
 * user routes
 */

Route::get('user/edit', 'UserController@edit');
Route::post('user/edit/{id}', 'UserController@update');

Route::get('user/dashboard/{id}', 'UserController@show');
Route::get('user/userShow/{id}', 'AnswerController@userShow');
Route::get('user/tickets', 'AnswerController@tickets');
Route::get('user/logout', 'UserController@logout');

Route::get('user/login', 'UserController@loginView');
Route::post('user/login', 'UserController@login');

Route::get('user/delete/{id}', 'UserController@delete');

Route::get('user/create', 'UserController@create');
Route::post('user/create', 'UserController@store');


/**
 *  categories routes
 */

Route::get('cat/delete/{id}', 'CategoryController@delete');

Route::get('cat/edit/{id}', 'CategoryController@edit');
Route::post('cat/edit/{id}', 'CategoryController@update');

Route::get('cat/create', 'CategoryController@create');
Route::post('cat/create', 'CategoryController@store');

Route::get('cat', 'CategoryController@index');


/**
 * questions routes
 */
Route::get('que', 'QuestionController@index');
Route::get('que/show/{id}', 'QuestionController@show');
Route::post('que/show', 'QuestionController@saveShow');

Route::get('que/upload', 'QuestionController@indexTest');
Route::post('que/upload', 'QuestionController@test');

Route::get('que/delete/{id}', 'QuestionController@delete');
Route::get('que/all/{id}', 'QuestionController@modifyQuestion');

Route::get('que/edit/{id}', 'QuestionController@edit');
Route::post('que/edit/{id}', 'QuestionController@update');

Route::get('que/create', 'QuestionController@create');
Route::post('que/create', 'QuestionController@store');


/**
 * admins routes
 */
Route::post('admin/edit', 'AdminController@update');
Route::get('admin/edit', 'AdminController@edit');

Route::post('admin/create', 'AdminController@store');
Route::get('admin/create', 'AdminController@create');
//for users who just want chat
Route::post('admin/chat/create', 'AdminController@storeOnlyChat');
Route::get('admin/chat/create', 'AdminController@createOnlyChat');


Route::get('admin/', 'AdminController@index');
Route::get('admin/delete/{id}', 'AdminController@delete');

Route::get('admin/login', 'AdminController@loginView');
Route::post('admin/login', 'AdminController@login');

Route::get('admin/logout', 'AdminController@logout');
Route::get('admin/links', 'AdminController@links');
Route::get('admin/chatLink/{token}', 'AdminController@chatLink');
Route::get('admin/showChatLink', 'AdminController@showChatLink');


Route::post('ans/reply', 'AnswerController@saveReply');


Route::get('ans/download/{file}', 'AnswerController@downloadItems');


// FIXME instead of resource, add all routes manually
//Route::resource('user', 'UserController');
//Route::resource('admin', 'AdminController');
//Route::resource('cat', 'CategoryController');
//Route::resource('que', 'QuestionController');
Route::resource('ans', 'AnswerController');

Route::get('request', 'ChatController@index');
Route::get('getUserRequest', 'ChatController@getUserRequest');
Route::get('status3', 'ChatController@status3');


/**
 * super admin
 */

Route::get('sp-admn/login', 'AdminController@spLogin');
Route::post('sp-admn/login', 'AdminController@spLogstore');

Route::get('sp-admn-chat', 'AdminController@allMainAdminsChat');
Route::get('sp-admn-sup', 'AdminController@allMainAdminsSupport');
Route::get('sp-admn/delete/{id}', 'AdminController@superAdminDelete');
Route::get('sp-admn/{id}', 'AdminController@mainAdminInfo');
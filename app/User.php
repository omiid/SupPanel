<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name',
        'email',
        'sub',
        'password',
        'username'
    ];
    public function scopeLogin($query, $email,$password)
    {
        $query->whereNotNull('email')
            ->whereNotNull('password')
            ->where([
                ['email', '=', $email],
                ['password', '=', $password]
            ]);
    }
}

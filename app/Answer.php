<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'option_id',
        'answer_group_id',
        'answer'

    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'sub',
        'avatar',
        'company',
        'token',
        'chat_only'
    ];
    public function category()
    {
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
    public function scopeLogin($query, $email,$password)
    {
        $query->whereNotNull('email')
            ->whereNotNull('password')
            ->where([
                ['email', '=', $email],
                ['password', '=', $password]
            ]);
    }
}

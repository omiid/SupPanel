<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequst extends Model
{
    protected $table = 'requests';
    protected $fillable = [
        'status'
    ];
}

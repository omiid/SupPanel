<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerGroups extends Model
{

    protected $table = 'answer_groups';

    protected $fillable = [
        'user_id',
        'value',
        'priority',
        'status',
    ];
}

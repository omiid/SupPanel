<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminCategory extends Model
{
    protected $table = 'admin_category';
    protected $fillable = ['category_id',
        'admin_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'sub'
    ];

    public function user()
    {
        return $this->belongsToMany('App\Admin')->withTimestamps();
    }
}

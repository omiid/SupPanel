<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = [
        'answer_group_id',
        'reply',
        'admin_id'
    ];
}

<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Answer;
use App\AnswerGroups;
use App\Category;
use App\Option;
use App\Panel;
use App\Question;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Echo_;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('IfAdminLogged', ['only' => ['create', 'store']]);
        $this->middleware('IfUserLogged', ['only' => ['index', 'show', 'saveShow']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if (Session::get('user')) {

                $sub = $this->getSubdomain($request);
                $id = Session::get('user');

                $mainAdmin = Admin::whereUsername($sub)->first();

                $user = User::whereId($id)->first();
                $name = $user->name;
                $ticketName = AnswerGroups::whereUserId($id)->get();
                $categories = Category::whereSub($sub)->get();
                return view('questions.index')
                    ->withName($name)
                    ->withToken($mainAdmin->token)
                    ->withUserav($user)
                    ->withTicketName($ticketName)
                    ->withCategories($categories);
            } else
                return redirect('user/login');
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        try {
            $sub = $this->getSubdomain($req);

            $nav_color = Panel::whereSub($sub)->pluck('color');

            $adminId = Session::get('admin');
            $admin = Admin::whereId($adminId)->first();
            $categories = Category::whereSub($sub)->pluck('name', 'id');
            if (count($categories) == 0) {
                return view('category.create')
                    ->withAdminav($admin)
                    ->withChatOnly($admin->chat_only)
                    ->with('noCategory', 'لطفا ابتدا دپارتمان مورد نظر را ایجاد کنید');
            }

            return view('questions.create')
                ->withAdminav($admin)
                ->withChatOnly($admin->chat_only)
                ->withCategories($categories)
                ->withColor($nav_color);
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $category_id = $input['category_id'];
            if (isset($input['question'])) {
                $questions = $input['question'];
                if (isset($questions['text'])) {
                    $texts = $questions['text'];
                    foreach ($texts as $item => $text) {
                        $save_question = new Question([
                            'category_id' => $category_id,
                            'question' => $text,
                            'type' => 1
                        ]);
                        $save_question->save();
                        $questionId = $save_question->id;
                        $option = new Option([
                            'question_id' => $questionId,
                            'value' => null
                        ]);
                        $option->save();

                    }
                }
                if (isset($questions['file'])) {
                    $files = $questions['file'];
                    foreach ($files as $item => $file) {
                        $save_question = new Question([
                            'category_id' => $category_id,
                            'question' => $file,
                            'type' => 5
                        ]);
                        $save_question->save();

                        $questionId = $save_question->id;
                        $option = new Option([
                            'question_id' => $questionId,
                            'value' => null
                        ]);
                        $option->save();

                    }
                }
                if (isset($input['option'])) {
                    $options = $input['option'];

                    if (isset($questions['check'])) {
                        $checks = $questions['check'];
                        foreach ($checks as $item => $check) {


                            $save_question = new Question([
                                'category_id' => $category_id,
                                'question' => $check,
                                'type' => 3
                            ]);
                            $save_question->save();
                            if ($save_question) {
                                $questionId = $save_question->id;
                                foreach ($options[$item] as $option) {
                                    $save_option = new Option([
                                        'question_id' => $questionId,
                                        'value' => $option
                                    ]);
                                    $save_option->save();
                                }
                            }

                        }
                    }

                    if (isset($questions['radio'])) {
                        $radios = $questions['radio'];
                        foreach ($radios as $item => $radio) {


                            $save_question = new Question([
                                'category_id' => $category_id,
                                'question' => $radio,
                                'type' => 4
                            ]);
                            $save_question->save();
                            if ($save_question) {
                                $questionId = $save_question->id;
                                foreach ($options[$item] as $option) {
                                    $save_option = new Option([
                                        'question_id' => $questionId,
                                        'value' => $option
                                    ]);
                                    $save_option->save();
                                }
                            }

                        }
                    }

                    if (isset($questions['select'])) {
                        $selects = $questions['select'];
                        foreach ($selects as $item => $select) {


                            $save_question = new Question([
                                'category_id' => $category_id,
                                'question' => $select,
                                'type' => 2
                            ]);
                            $save_question->save();
                            if ($save_question) {
                                $questionId = $save_question->id;
                                foreach ($options[$item] as $option) {
                                    $save_option = new Option([
                                        'question_id' => $questionId,
                                        'value' => $option
                                    ]);
                                    $save_option->save();
                                }
                            }

                        }
                    }
                }
            }
            return redirect()->back()->withSuccess('سوالات با موفقیت ثبت شدند');
//            $input = $request->all();
//            $categoryId = $input['category'];
//            $data = $input['data'];
//            $json_decode = json_decode(stripslashes($data));
//            $types = array();
//            $values = array();
//            foreach ($json_decode as $item) {
//
//                // add types of input without qoutation (trim) to $types array
//                $type = trim(json_encode($item->type), '"');
//                $input_question = $item->label;
////                trim(json_encode($item->label), '"');
//
//                if ($type == 'text')
//                    $type = 1;
//                elseif ($type == 'select')
//                    $type = 2;
//                elseif ($type == 'checkbox-group')
//                    $type = 3;
//                elseif ($type == 'radio-group')
//                    $type = 4;
//                elseif ($type == 'file')
//                    $type = 5;
//
//
//                $question = new Question([
//                    'category_id' => $categoryId,
//                    'question' => $input_question,
//                    'type' => $type
//                ]);
//                $question->save();
//                $questionId = $question->id;
//
////            array_push($values, $question);
////            array_push($values, $type);
//
//                // add options of radio button
//                if ($type == 2 || $type == 3 || $type == 4) {
//                    foreach ($item->values as $value) {
//                        $option = new Option([
//                            'question_id' => $questionId,
//                            'value' => $value->label
//                        ]);
//                        $option->save();
//                    }
//                }
//                if ($type == 1 || $type == 5) {
//                    $option = new Option([
//                        'question_id' => $questionId,
//                        'value' => null
//                    ]);
//                    $option->save();
//
//                }
//
//            }
//
//            return 'done';
        } catch
        (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }

//        $input = $request->all();
//        $categoryId = $input['category_id'];
//
//        $optionsRequest = $input['option'];
//        $questionRequest = $input['question'];
//        $typeRequest = $input['type'];
//
//
//        for ($i = 0; $i < count($questionRequest); $i++) {
//            $question = new Question([
//                'category_id' => $categoryId,
//                'question' => $questionRequest[array_keys($questionRequest)[$i]],
//                'type' => $typeRequest[array_keys($typeRequest)[$i]]
//            ]);
//            $question->save();
//            $questionId = $question->id;
//
//            if (count($optionsRequest) > 0) {
//                foreach ($optionsRequest[array_keys($optionsRequest)[$i]] as $item) {
//                    $option = new Option([
//                        'question_id' => $questionId,
//                        'value' => $item
//                    ]);
//                    $option->save();
//                }
//            }
//        }
//
//        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
        try {
            $sub = $this->getSubdomain(\request());
            $userId = Session::get('user');
            $user = User::whereId($userId)->first();
            $questionId = Question::whereCategoryId($id)->pluck('id')->toArray();
            $options = Option::whereIn('question_id', $questionId)->get();
            $questions = Question::whereCategoryId($id)->get();
            $ticketName = AnswerGroups::whereUserId($id)->get();
            $mainAdmin = Admin::whereUsername($sub)->first();


            if (!$questionId) {
                return view('questions.show')
                    ->withEmptyPage('1')
                    ->withToken($mainAdmin->token)
                    ->withUserav($user)
                    ->withTicketName($ticketName);
            }
            return view('questions.show')
                ->withOptions($options)
                ->withQuestions($questions)
                ->withUserav($user)
                ->withToken($mainAdmin->token)
                ->withTicketName($ticketName);
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public
    function saveShow(Request $request)
    {
        try {
//
            /**
             * images validation
             */

            $images = $request->file('file');
            if (!empty($images)) {
                foreach ($images as $key => $image) // add individual rules to each image
                {
                    $this->validate($request, [sprintf('file.%d', $key) => 'image|required|mimes:jpeg,png,jpg,svg|max:2048']);
                }
            }
            /**
             * field require validation
             */

            $this->validate($request, [
                'subject' => 'required',

            ],
                [
                    'subject.required' => 'لطفا موضوع را پر کتید',
                ]);
            $input = $request->all();
            if (Session::get('user')) {
                $userId = Session::get('user');

                // each user can add 30 tickets (spam)
                $userTicketNumber = count(AnswerGroups::whereUserId($userId)->get());
                if ($userTicketNumber == 30 || $userTicketNumber > 30)
                    return 'you are spamming :|:|:|:|:|:|';

                $files = $request->file('file');

                $answerGroup = AnswerGroups::create([
                    'user_id' => $userId,
                    'value' => $input['subject'],
                    'status' => 0,
                    'priority' => $input['priority']
                ]);

                $answerGroupId = $answerGroup->id;
                if ($files) {
                    foreach ($files as $key => $file) {

//                        $this->validate($request, ['file[42]' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);


                        $fileName = $key . time() . '.' . $file->getClientOriginalName();
                        $destinationPath = 'uploads';
                        $file->move($destinationPath, $fileName);


                        $answerImage = new Answer([
                            'option_id' => $key,
                            'answer_group_id' => $answerGroupId,
                            'answer' => $fileName
                        ]);
                        $answerImage->save();
                    }
                }
                //        for($i = 0; $i < count($input['count']); $i++) {
                //            $file = $files[$i];
                //            $fileName = $i . time() . '.' .  $file->getClientOriginalName();
                //            $destinationPath = 'uploads';
                //            $file->move($destinationPath,$fileName);
                //
                //
                //            $answerImage = new Answer([
                //                'option_id' => $imageOptionId,
                //                'answer_group_id' => $answerGroupId,
                //                'answer' => $fileName
                //            ]);
                //            $answerImage->save();
                //
                //
                //        }

                if (isset($input['text']) && $input['text'] != NULL && $input['text'] != '' && count($input['text']) > 0) {
                    foreach ($input['text'] as $key => $inputText) {
                        if ($inputText != null) {
                            $answerText = new Answer([
                                'option_id' => $key,
                                'answer_group_id' => $answerGroupId,
                                'answer' => $inputText
                            ]);
                            $answerText->save();
                        }
                    }
                }

                if (isset($input['checkbox']) && count($input['checkbox']) > 0) {
                    foreach ($input['checkbox'] as $key => $inputText) {
                        $answerCheckBox = new Answer([
                            'option_id' => $key,
                            'answer_group_id' => $answerGroupId,
                            'answer' => null
                        ]);
                        $answerCheckBox->save();
                    }
                }
                if (isset($input['radio']) && count($input['radio']) > 0) {
                    foreach ($input['radio'] as $key => $inputText) {
                        $answerRadio = new Answer([
                            'option_id' => $inputText,
                            'answer_group_id' => $answerGroupId,
                            'answer' => null
                        ]);
                        $answerRadio->save();
                    }
                }
                if (isset($input['select']) && count($input['select']) > 0) {
                    foreach ($input['select'] as $key => $inputText) {
                        $answerSelect = new Answer([
                            'option_id' => $inputText,
                            'answer_group_id' => $answerGroupId,
                            'answer' => null
                        ]);
                        $answerSelect->save();
                    }
                }
                return redirect('que');

            } else
                return redirect('user/login');
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * show all questions related to specific cat to admin
     * in order to delete or edit it
     */
    public
    function modifyQuestion($id)
    {
        try {
            $adminId = Session::get('admin');
            $admin = Admin::whereId($adminId)->first();
            $questionId = Question::whereCategoryId($id)->pluck('id')->toArray();
            $options = Option::whereIn('question_id', $questionId)->get();
            $questions = Question::whereCategoryId($id)->get();
            return view('questions.allQuestions')
                ->withChatOnly($admin->chat_only)
                ->withAdminav($admin)
                ->withOptions($options)
                ->withQuestions($questions);
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {

        // fixme check if admin is the right person to edit the question
        try {
            $adminId = Session::get('admin');
            $admin = Admin::whereId($adminId)->first();
            $question = Question::whereId($id)->first();
            $options = Option::whereQuestionId($id)->get();

            return view('questions.edit')
                ->withChatOnly($admin->chat_only)
                ->withQuestion($question)->withAdminav($admin)
                ->withOptions($options);
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'question' => 'required',
            ],
                [
                    'question.required' => 'لطفا همه ی فیلدها را پر کنید',
                ]);
            $input = $request->all();
            Question::whereId($id)->update(['question' => $input['question']]);
            $question = Question::whereId($id)->get();
            if (isset($input['option'])) {
                $options = $input['option'];

                foreach ($options as $optionId => $option) {
                    Option::whereId($optionId)->update(['value' => $option]);
                }
            }
            return redirect('que/all/' . $question[0]->category_id);
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function delete($id)
    {
        try {
            $sub = $this->getSubdomain(\request());
            $cat_id = Question::whereId($id)->pluck('category_id');
            $cat_id = $cat_id[0];
            $cat = Category::whereId($cat_id)->pluck('sub');
            $cat = $cat[0];
            if ($cat == $sub) {
                Question::whereId($id)->delete();
                return redirect()->back();
            } else return 'selected question is wrong, try again';
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    public
    function indexTest()
    {

        return view('questions.test');
    }

    public
    function test(Request $request)
    {
        $file = public_path() . "/uploads/jdom_license.txt";
        return response()->download($file);
    }

    /**
     * get subdomain
     * return a name
     */
    private
    function getSubdomain(Request $request)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        if (count($url_array) == 3)
            $subdomain = $url_array[0];
        else $subdomain = null;

        return $subdomain;
    }
}


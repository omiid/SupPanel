<?php

namespace App\Http\Controllers;

use App\Panel;
use Illuminate\Http\Request;

class PanelController extends Controller
{

    /**
     * @param Request $request
     * save color
     */
    public function saveColor(Request $request)
    {
        $subdomain = $this->getSubdomain($request);
        $color = $request->color;
        $existed_subdomain = Panel::whereSub($subdomain)->pluck('sub');
        if(isset($existed_subdomain[0]))
            Panel::whereSub($subdomain)->update(['color' => $color]);
        else {
            Panel::create([
                'color' => $color,
                'sub' => $subdomain
            ]);
        }
        return redirect()->back();
    }


    /**
     * get subdomain
     * return a name
     */
    private function getSubdomain(Request $request) {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        if(count($url_array) == 3)
            $subdomain = $url_array[0];
        else $subdomain = null;

        return $subdomain;
    }
}

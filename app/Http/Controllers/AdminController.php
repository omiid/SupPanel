<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use App\Panel;
use App\User;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('IfAdminLogged',
            ['except' => ['testfunction',
                'loginView',
                'login',
                'spLogin',
                'spLogstore',
                'test',
                'testVie',
                'create',
                'store',
                'mainAdminInfo',
                'superAdminDelete',
                'allMainAdminsChat',
                'allMainAdminsSupport',
                'storeOnlyChat',
                'createOnlyChat',
                'url'
            ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $adminId = Session::get('admin');

            $adminAv = Admin::whereId($adminId)->first();

            $sub = $this->getSubdomain($request);

            $sub = $sub ? $sub : -1;

            $nav_color = Panel::whereSub($sub)->pluck('color');

            $admins = Admin::whereSub($sub)->latest('created_at')->get();
            return view('admins.index')
                ->withAdmins($admins)
                ->withChatOnly($adminAv->chat_only)
                ->withAdminav($adminAv)
                ->withColor($nav_color);

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }


    public function createOnlyChat(Request $request)
    {
        try {
            $sub = $this->getSubdomain($request);
            $mainAdmin = $sub ? 1 : 0;

            if ($mainAdmin == 1) {
                $adminId = Session::get('admin');
                if ($adminId != null) {
                    $admin = Admin::whereId($adminId)->first();

                    $adminSubdomain = Admin::whereUsername($sub)->first();
                    $adminSubdomain = $adminSubdomain['id'];
                    // check if the logged in user with subdomain matches
                    if ($adminId == $adminSubdomain) {
                        return view('admins.create')
                            ->withAdminav($admin)
                            ->withChatOnly(1)
                            ->withMainAdmin($mainAdmin);
                    }
                    return 'you are using wrong account, please login again';

                } else {
                    return redirect('admin/login');
                }
            }
            return view('admins.main-admin-create')
                ->withChatOnly(1);

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }


    public function storeOnlyChat(Request $request)
    {
        try {
            $subdomain = $this->getSubdomain($request);

            // validating admin entered password and email and usernameor not
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
                'username' => 'required'

            ], [
                'email.required' => 'لطفا ایمیل را وارد کنید',
                'email.email' => 'ایمیل معتبر نیست',
                'password.required' => 'لطفا کلمه ی عبور را وارد کنید',
                'username.required' => 'لطفا نام کاربری را وارد کنید'

            ]);

            if ($subdomain == null) {
                $this->validate($request, [
                    'company' => 'required'
                ], [
                    'company.required' => 'لطفا نام شرکت را وارد کنید',
                ]);
            }
            // end validate

            $input = $request->all();
            $inputAdmin = $input;
            try {
                $mainAdminEmail = Admin::whereUsername($subdomain)->first();
                $mainAdminEmail = $mainAdminEmail['email'];
                $adminsUsername = Admin::whereSub($subdomain)->pluck('username')->toArray();
                $adminsEmail = Admin::whereSub($subdomain)->orWhere('email', $mainAdminEmail)->pluck('email')->toArray();
                if (isset($inputAdmin['name']))
                    $name = $inputAdmin['name'];
                else
                    $name = null;
                if (in_array($inputAdmin['username'], $adminsUsername) || in_array($inputAdmin['email'], $adminsEmail)) {
                    return Redirect()->back()->with('user or mail exists', 'این نام کاربری یا ایمیل قبلا در سیستم ثبت شده است');
                } else {
                    $admin = Admin::create([
                        'name' => $name,
                        'username' => $inputAdmin['username'],
                        'email' => $inputAdmin['email'],
                        'password' => $inputAdmin['password'],
                        'sub' => $subdomain,
                        'chat_only' => 1
                    ]);
                    if ($subdomain != null) {
                        $admin->category()->attach($input['category']);
                        return redirect()->back()->with('stored', 'پشتیبان اضافه شد');
                    } else {


                        $token = $this->random_str(20);
                        while (true) {
                            $existed_token = Admin::whereToken($token)->first();
                            if (count($existed_token) > 0)
                                $token = $this->random_str(10);
                            else break;
                        }
                        $add_company = Admin::whereId($admin->id)->update([
                            'company' => $inputAdmin['company'],
                            'token' => $token
                        ]);

                        return Redirect('http://' .
                            $inputAdmin['username'] .
                            '.supportpanel.net/panel/cat');
                    }
                }

            } catch (\Illuminate\Database\QueryException $ex) {

                return Redirect('admin/create')->with('wrong-reg', 'ایمیل تکراری است');
            }

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $sub = $this->getSubdomain($request);
            $mainAdmin = $sub ? 1 : 0;

            if ($mainAdmin == 1) {
                $adminId = Session::get('admin');
                if ($adminId != null) {
                    $admin = Admin::whereId($adminId)->first();

                    $adminSubdomain = Admin::whereUsername($sub)->first();
                    $adminSubdomain = $adminSubdomain['id'];
                    // check if the logged in user with subdomain matches
                    if ($adminId == $adminSubdomain) {

                        $chat_only = $admin->chat_only;
                        $categories = Category::whereSub($sub)->pluck('name', 'id');

                        if (count($categories) == 0) {
                            return view('category.create')
                                ->withChatOnly($admin->chat_only)
                                ->with('noCategory', 'لطفا ابتدا دپارتمان مورد نظر را ایجاد کنید');
                        }
                        return view('admins.create')
                            ->withCategories($categories)
                            ->withAdminav($admin)
                            ->withChatOnly($admin->chat_only)
                            ->withMainAdmin($mainAdmin);
                    }
                    return 'you are using wrong account, please login again';

                } else {
                    return redirect('admin/login');
                }
            }
            return view('admins.main-admin-create');

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $subdomain = $this->getSubdomain($request);

            // validating admin entered password and email and usernameor not
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required',
                'username' => 'required'

            ], [
                'email.required' => 'لطفا ایمیل را وارد کنید',
                'email.email' => 'ایمیل معتبر نیست',
                'password.required' => 'لطفا کلمه ی عبور را وارد کنید',
                'username.required' => 'لطفا نام کاربری را وارد کنید'

            ]);

            if ($subdomain != null) {
                $admin_chat_only = Admin::whereUsername($subdomain)->first();
                $admin_chat_only = $admin_chat_only['chat_only'];
                $this->validate($request, [
                    'category' => 'required'
                ], [
                    'category.required' => 'لطفا نام دپارتمان را وارد کنید'
                ]);
            } else $admin_chat_only = 0;

            if ($subdomain == null) {
                $this->validate($request, [
                    'company' => 'required'
                ], [
                    'company.required' => 'لطفا نام شرکت را وارد کنید',
                ]);
            }
            // end validate

            $input = $request->all();
            $inputAdmin = $input;
//            try {
            $mainAdminEmail = Admin::whereUsername($subdomain)->first();
            $mainAdminEmail = $mainAdminEmail['email'];
            $adminsUsername = Admin::whereSub($subdomain)->pluck('username')->toArray();
            $adminsEmail = Admin::whereSub($subdomain)->orWhere('email', $mainAdminEmail)->pluck('email')->toArray();
            if (isset($inputAdmin['name']))
                $name = $inputAdmin['name'];
            else
                $name = null;
            if (in_array($inputAdmin['username'], $adminsUsername) || in_array($inputAdmin['email'], $adminsEmail)) {
                return Redirect()->back()->with('user or mail exists', 'این نام کاربری یا ایمیل قبلا در سیستم ثبت شده است');
            } else {
                $admin = Admin::create([
                    'name' => $name,
                    'username' => $inputAdmin['username'],
                    'email' => $inputAdmin['email'],
                    'password' => $inputAdmin['password'],
                    'sub' => $subdomain,
                    'chat_only' => $admin_chat_only

                ]);
                if ($subdomain != null) {
                    $admin->category()->attach($input['category']);
                    return redirect()->back()->with('stored', 'پشتیبان اضافه شد');
                } else {
                    $token = $this->random_str(20);
                    while (true) {
                        $existed_token = Admin::whereToken($token)->first();
                        if (count($existed_token) > 0)
                            $token = $this->random_str(10);
                        else break;
                    }
                    $add_company = Admin::whereId($admin->id)->update([
                        'company' => $inputAdmin['company'],
                        'token' => $token
                    ]);
                    return Redirect('http://' .
                        $inputAdmin['username'] .
                        '.supportpanel.net/panel/cat');
                }
                return redirect()->back()->with('stored', 'پشتیبان اضافه شد');

            }

//            } catch (\Illuminate\Database\QueryException $ex) {
//
//                return Redirect('admin/create')->with('wrong-reg', 'ایمیل تکراری است');
//            }

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function edit()
    {
        try {
            $adminId = Session::get('admin');
            if ($adminId == null)
                return redirect('admin/login');

            $admin = Admin::whereId($adminId)->first();
            if ($admin['sub'] == null) {
                $main_admin = 1;
            } else $main_admin = null;
            return view('admins.edit')
                ->withAdminav($admin)
                ->withAdmin($admin)
                ->withChatOnly($admin->chat_only)
                ->withMainAdmin($main_admin);

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request)
    {
        try {
            $this->validate($request, [
                'admin.email' => 'required|email',
                'admin.password' => 'required',
                'admin.name' => 'required',

            ], [
                'admin.email.required' => 'لطفا ایمیل را وارد کنید',
                'admin.email.email' => 'ایمیل معتبر نیست',
                'admin.password.required' => 'لطفا کلمه ی عبور را وارد کنید',
                'admin.name.required' => 'لطفا نام را وارد کنید',
            ]);

            $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
            try {
                $id = Session::get('admin');
                $input = $request->all();

                $main_admin = Admin::whereId($id)->first();
                if ($main_admin->sub == null)// which means he/she is the main admin
                {

                    $this->validate($request, [
                        'admin.company' => 'required',

                    ], [
                        'admin.company.required' => 'لطفا شرکت را وارد کنید',
                    ]);
                }

                if ($request->file('file') != null) {

                    $file = $request->file('file');
//                   {
//                        return redirect()->back()->with('image_error', 'عکس اپلود شده دارای مشکل است،‌مجددا تلاش کنید');
//                    }
//                    else {
                    $fileName = time() . '.' . $file->getClientOriginalName();
                    $destinationPath = 'avatars';
                    $file->move($destinationPath, $fileName);

                    $update_image = Admin::whereId($id)->update(['avatar' => $fileName]);
//                    }
                }


                Admin::whereId($id)->update($input['admin']);
                return redirect('admin/edit')->with('edited', 'ویرایش با موفقیت انجام شد');

            } catch (\Illuminate\Database\QueryException $ex) {

                return Redirect()->back()->with('catch',
                    'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
            }

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }


    /**
     * delete admin
     */
    public
    function delete($id)
    {
        try {

            $adminId = Session::get('admin');

            $admin = Admin::whereId($adminId)->first();
            $subdomain = $this->getSubdomain(request());

            $adminUsername = $admin->username;

            if ($adminUsername == $adminUsername) {
                Admin::whereId($id)->delete();

            }
            return redirect('admin')
                ->withAdminav($admin);

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public
    function logout()
    {
        try {
            $subdomain = $this->getSubdomain(request());
            Session::forget('admin');
            return Redirect('http://' . $subdomain . '.supportpanel.net/panel/admin/login');

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * view login page
     */
    public
    function loginView()
    {
        try {
            $subdomain = $this->getSubdomain(request());

            $sub = Admin::whereUsername($subdomain)->first();
            if (!$subdomain || count($sub) < 1)
                return 'WRONG :|||||||||||||||';

            $company = Admin::whereUsername($subdomain)->pluck('company');
            $company = $company[0];
            return view('admins.login')
                ->withCompany($company);

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *  login admin
     */
    public
    function login(Request $request)
    {
        try {
            $input = $request->all();
            $subdomain = $this->getSubdomain($request);
            $query = Admin::login($input['email'], $input['password'])->whereSub($subdomain)->first();
            if (!$query)
                $query = Admin::login($input['email'], $input['password'])->whereUsername($subdomain)->whereSub(null)->first();
            if ($query) {
                $username = $query->sub ? $query->sub : $query->username;

                Session::put('admin', $query->id);
                Session::save();

                if ($query->sub != null) {
                    return Redirect('http://' . $username . '.supportpanel.net/panel/ans')
                        ->withChatOnly($query->chat_only);
                } else {
                    return Redirect('http://' . $username . '.supportpanel.net/panel/cat')
                        ->withChatOnly($query->chat_only);
                }
            } else {
                return Redirect('http://' . $subdomain . '.supportpanel.net/panel/login')
                    ->with('wrong-email', 'ایمیل وارد شده یا لینک ورودی اشتباه است');
            }

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

// FIXME if we want to add a new admin, we should be registered + make different page for main-admin

    /**
     * get subdomain
     * return a name
     */
    private
    function getSubdomain(Request $request)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        if (count($url_array) == 3)
            $subdomain = $url_array[0];
        else $subdomain = null;

        return $subdomain;
    }

    public
    function links(Request $request)
    {
        try {
            $adminId = Session::get('admin');
            $admin = Admin::whereId($adminId)->first();

            $username = Admin::whereId($adminId)->first();
            $username = $username['username'];
            $nav_color = Panel::whereSub($username)->pluck('color');
            $userLogin = 'http://' . $username . '.supportpanel.net/panel/user/login';
            $adminLogin = 'http://' . $username . '.supportpanel.net/panel/admin/login';
            return view('admins.links')->withUserLogin($userLogin)
                ->withAdminLogin($adminLogin)
                ->withAdminav($admin)
                ->withChatOnly($admin->chat_only)
                ->withColor($nav_color);

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    public
    function testVie()
    {

        return redirect('www.google.com');

        return view('test');
    }

    public
    function test(Request $request)
    {
        return 'falseeee';
        $input = $request->all();
        $data = $input['data'];
        $json_decode = json_decode(stripslashes($data));
//        return json_encode($json_decode);
        $types = array();
        $values = array();
        foreach ($json_decode as $item) {
            // add types of input without qoutation (trim) to $types array
            $type = trim(json_encode($item->type), '"');
            $question = trim(json_encode($item->label), '"');
            array_push($values, $question);
            array_push($values, $type);

            // add options of radio button
            if ($type == 'radio-group' || $type == 'checkbox-group' || $type == 'select') {
                foreach ($item->values as $value)
                    array_push($values, $value->label);
            }
        }

        return $values;
    }


    /**
     * Shahab's panel
     */


    public
    function allMainAdminsChat()
    {
        if (Session::get('adminSP')) {
            $mains = Admin::whereNull('sub')->whereNotNull('company')->whereChatOnly(1)->get();

            return view('super-admin.mainadmins')
                ->withMains($mains);
        }
        return redirect('sp-admn/login');
    }

    public
    function allMainAdminsSupport()
    {
        if (Session::get('adminSP')) {
            $mains = Admin::whereNull('sub')->whereNotNull('company')->get();

            return view('super-admin.mainadmins')
                ->withMains($mains);
        }
        return redirect('sp-admn/login');
    }

    public
    function mainAdminInfo($id)
    {
        if (Session::get('adminSP')) {
            $admin = Admin::whereId($id)->first();

            $users_count = User::whereSub($admin->username)->count();
            $admins = Admin::whereSub($admin->username)->get();
//return $admin;
            return view('super-admin.adminInfo')
                ->withAdmin($admin)
                ->withUsersCount($users_count)
                ->withAdmins($admins);
        }
        return redirect('sp-admn/login');
    }

    public
    function superAdminDelete($id)
    {
        if (Session::get('adminSP')) {
            Admin::whereId($id)->delete();
            return redirect()->back();
        }
        return redirect('sp-admn/login');
    }

    public function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }


    public function showChatLink()
    {

        $admin_id = Session::get('admin');
        $admin = Admin::whereId($admin_id)->first();
        $token = $admin->token;
        $subdomain = $this->getSubdomain(\request());


        return view('admins.show-chat-link')
            ->withChatOnly($admin->chat_only)
            ->withSub($subdomain)
            ->withToken($token);
    }


    public function chatLink($token)
    {
        try {
            if ($token && $token != null) {
                return view('admins.chat-link')
                    ->withToken($token);
            } else    return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');

        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }

    public function url()
    {
        return redirect()->away('https://www.dropbox.com');
        return redirect('http://www.google.com');
    }

    public function spLogin()
    {
        return view('sp-login');
    }

    public function spLogstore(Request $request)
    {
        $input = $request->all();
        if ($input['email'] == 'spfd_root@support.com' && $input['password'] == 'bpT_M98r#A2\by)@') {

            Session::put('adminSP', 'spfd_root@support.com');
            Session::save();
            return redirect('sp-admn-chat');

        } else return 'email or password is wrong';
    }
}

<?php

namespace App\Http\Controllers;

use App\AdminCategory;
use App\Answer;
use App\Admin;
use App\AnswerGroups;
use App\Category;
use App\Option;
use App\Question;
use App\Reply;
use App\User;
use App\UserCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('admin')) {
            $sub = $this->getSubdomain(request());
            $dbSubdomain = Admin::whereSub(null)->pluck('username')->toArray();
            if (!$sub || !in_array($sub, $dbSubdomain))
                return 'لینک نامعتبر';
            $adminId = Session::get('admin');

            $admin = Admin::whereId($adminId)->first();
            if ($admin->chat_only == 1) {
                return redirect('request');
            }

            $categories = AdminCategory::whereAdminId($adminId)->pluck('category_id')->toArray();

            $questions = Question::whereIn('category_id', $categories)->pluck('id')->toArray();

            $options = Option::whereIn('question_id', $questions)->pluck('id')->toArray();

            $answers = Answer::whereIn('option_id', $options)->distinct()->pluck('answer_group_id')->toArray();

            $answerGroups = AnswerGroups::whereIn('id', $answers)->whereStatus(0)->get();
//return $admin;
            return view('answers.index')
                ->withAnswerGroups($answerGroups)
                ->withAdminav($admin)
                ->withChatOnly($admin->chat_only);
        } else
            return redirect('admin/login');
    }
    //FIXME make a middleware that main admin shouldn't see this parts and admins can't see main admin's

    /**
     *  all user tickets
     */

    public function tickets()
    {
        if (Session::get('user')) {
            $id = Session::get('user');
            $sub = $this->getSubdomain(\request());
            $mainAdmin = Admin::whereUsername($sub)->first();
            $user = User::whereId($id)->first();
            $name = $user->name;
            $ticketName = AnswerGroups::whereUserId($id)->get();
            $categories = Category::all();

            return view('answers.tickets')
                ->withName($name)
                ->withUserav($user)
                ->withToken($mainAdmin->token)
                ->withTicketName($ticketName)
                ->withCategories($categories);
        } else
            return redirect('user/login');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adminId = Session::get('admin');

        if ($adminId) {

            $admin = Admin::whereId($adminId)->first();
            $categories = AdminCategory::whereAdminId($adminId)->pluck('category_id')->toArray();

            $questionsId = Question::whereIn('category_id', $categories)->pluck('id')->toArray();
            $questions = Question::whereIn('category_id', $categories)->get();

            $optionsId = Option::whereIn('question_id', $questionsId)->pluck('id')->toArray();

            $options = Option::whereIn('question_id', $questionsId)->get();

            $answers = Answer::whereIn('option_id', $optionsId)->whereAnswerGroupId($id)->get();

            $answerGroup = AnswerGroups::whereId($id)->get();
        }
        return view('answers.show')
            ->withAnswers($answers)
            ->withQuestions($questions)
            ->withAdminav($admin)
            ->withChatOnly($admin->chat_only)
            ->withOptions($options)
            ->withAnswerGroup($answerGroup)
            ->withTicketName($answerGroup);

    }

    /**
     * download items from answer/show.blade.php
     */
    public function downloadItems($fileName)
    {
        $file = public_path() . "/uploads/" . $fileName;
        return response()->download($file);

    }


    /**
     * save reply from show method
     */

    public function saveReply(Request $request)
    {
        // TODO  edit "No data available in table" in jquery/database (users.tickets)
        if (Session::get('admin')) {

            /**
             * field require validation
             */

            $this->validate($request, [
                'reply' => 'required',

            ],
                [
                    'reply.required' => 'put some value in reply field',
                ]);
            $input = $request->all();
            $reply = $input['reply'];
            $answerGroupId = key($input['groupId']);
            $adminId = Session::get('admin');
            AnswerGroups::whereId($answerGroupId)->update(['status' => 1]);

            Reply::create([
                'admin_id' => $adminId,
                'reply' => $reply,
                'answer_group_id' => $answerGroupId
            ]);
            return redirect('ans');
        } else
            return redirect('admin/login');
    }

    /**
     * show selected ticket
     * @param $id
     * @return mixed
     */
    public function userShow($id)
    {
        // get id of answer group
        $userId = Session::get('user');
        $optionIds = Answer::where('answer_group_id', $id)->pluck('option_id')->toArray();
        $answers = Answer::where('answer_group_id', $id)->get();
        $questionIds = Option::whereIn('id', $optionIds)->pluck('question_id')->toArray();
        $options = Option::whereIn('id', $optionIds)->get();
        $questions = Question::whereIn('id', $questionIds)->get();
        $answerGroup = AnswerGroups::whereId($id)->get();
        $sub = $this->getSubdomain(\request());

        $mainAdmin = Admin::whereUsername($sub)->first();

        $reply = Reply::whereAnswerGroupId($id)->get();
        $ticketName = AnswerGroups::whereUserId($userId)->get();

        return view('answers.ticket')
            ->withQuestions($questions)
            ->withToken($mainAdmin->token)
            ->withAnswers($answers)
            ->withOptions($options)
            ->withAnswerGroup($answerGroup)
            ->withReply($reply)
            ->withTicketName($ticketName);
    }

    private function getSubdomain(Request $request)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        if (count($url_array) == 3)
            $subdomain = $url_array[0];
        else $subdomain = null;

        return $subdomain;
    }
}

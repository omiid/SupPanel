<?php

namespace App\Http\Controllers;

use App\Admin;
use App\AdminCategory;
use App\Http\Controllers\Controller;
use App\UserRequst;
use Illuminate\Http\Request;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;

class ChatController extends Controller
{
    public function index()
    {
        if (Session::get('admin')) {
            $adminid = Session::get('admin');
            $adminav = Admin::whereId($adminid)->first();

            return view('admins.requests')
                ->withChatOnly($adminav->chat_only)
                ->withAdminav($adminav);
        } else {
            return redirect('admin/login');
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getUserRequest(Request $request)
    {
        if (Session::get('admin')) {
            $adminId = Session::get('admin');
        } else {
            return redirect('admin/login');
        }
        $ids = json_decode(stripslashes($request->data));

        $department = AdminCategory::whereAdminId($adminId)->pluck('category_id');

        $requests = UserRequst::where('status', '!=', 1)
            ->whereIn('department', $department)
            ->whereNotIn('id', $ids)
            ->get();
        return response()->json($requests);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public
    function status3(Request $request)
    {
        $id = json_decode(stripslashes($request->data));

        $requests = UserRequst::whereId($id)->update(['status' => 3]);

        return response()->json($requests);
    }


}
















































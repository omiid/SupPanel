<?php

namespace App\Http\Controllers;

use App\Admin;
use App\AnswerGroups;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('IfUserLogged', ['only' => ['logout']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sub = $this->getSubdomain($request);
        $dbSubdomain = Admin::whereSub(null)->pluck('username')->toArray();
        if (!$sub || !in_array($sub, $dbSubdomain))
            return 'لینک نامعتبر';
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'user.username' => 'required|max:40',
            'user.email' => 'required|max:40|email',
            'user.password' => 'required|max:40',

        ],


            [
                'user.username.required' => 'لطفا نام کاربری را وارد کنید',
                'user.email.required' => ' لطفا ایمیل را وارد کنید',
                'user.password.required' => 'لطفا کلمه ی عبور را وارد کنید',

                'user.email.email' => 'ایمیل معتبر نیست',

                'user.username.max' => 'تعداد کاراکتر بیش از حد مجاز است',
                'user.email.max' => 'تعداد کاراکتر بیش از حد مجاز است',
                'user.password.max' => 'تعداد کاراکتر بیش از حد مجاز است',

            ]);
        $input = $request->all();
        $inputUser = $input['user'];
        $sub = $this->getSubdomain($request);

        try {

            $usersEmail = User::whereSub($sub)->pluck('email')->toArray();
            if (in_array($inputUser['email'], $usersEmail)) {
                return Redirect()->back()->with('user or mail exists', 'این نام کاربری یا ایمیل قبلا در سیستم ثبت شده است');
            } else {

                $query = User::create([
                    'username' => $inputUser['username'],
                    'email' => $inputUser['email'],
                    'password' => $inputUser['password'],
                    'sub' => $sub
                ]);
                if ($query)
                    return redirect('user/login');
            }
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect('user/create')->with('wrong-reg', 'ایمیل تکراری است');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::whereId($id)->first();
        $name = $user->name;
        $ticketName = AnswerGroups::whereUserId($id)->get();

        return view('users.dashboard')
            ->withName($name)
            ->withTicketName($ticketName);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        $userId = Session::get('user');
        if ($userId == null)
            return redirect('user/login');
        $ticketName = AnswerGroups::whereUserId($userId)->get();

        $sub = $this->getSubdomain(\request());
        $mainAdmin = Admin::whereUsername($sub)->first();

        $user = User::whereId($userId)->first();

        return view('users.edit')
            ->withUser($user)
            ->withToken($mainAdmin->token)
            ->withUserav($user)
            ->withTicketName($ticketName);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $this->validate($request, [
            'user.email' => 'required|email',
            'user.password' => 'required',
            'user.username' => 'required',
            'user.name' => 'required',

        ], [


            'user.email.required' => ' لطفا ایمیل را وارد کنید',
            'user.email.email' => 'ایمیل معتبر نیست',
            'user.password.required' => 'لطفا کلمه ی عبور را وارد کنید',
            'user.name.required' => 'لطفا نام  را وارد کنید',
            'user.username.required' => '   لطفا نام کاربری را وارد کنید',

        ]);

        $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);

        if ($request->file('file') != null) {
            $file = $request->file('file');
            $fileName = time() . '.' . $file->getClientOriginalName();
            $destinationPath = 'avatars';
            $file->move($destinationPath, $fileName);

            $update_image = User::whereId($id)->update(['avatar' => $fileName]);

        }

        $input = $request->all();
        User::whereId($id)->update($input['user']);
        return redirect()->back()->with('edited', 'ویرایش با موفقیت انجام شد');
    }


    /**
     * Remove the specified resource from storage.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $sub = $this->getSubdomain(\request());

        Session::forget('user');
        return Redirect('http://' . $sub . '.supportpanel.net/panel/user/login');
    }

    public function loginView()
    {
        $subdomain = $this->getSubdomain(request());
        $sub = Admin::whereUsername($subdomain)->first();
        if (!$subdomain || count($sub) < 1)
            return 'WRONG :|||||||||||||||';

        return view('users.login');
    }

    public function login(Request $request)
    {

        /**
         * field require validation
         */

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',

        ],
            [
                'email.required' => 'put some value in email field',
                'password.required' => 'put some value in password field',
                'email.email' => 'enter valid mail',
            ]);

        $input = $request->all();
        $sub = $this->getSubdomain($request);

        $query = User::login($input['email'], $input['password'])->whereSub($sub)->first();
        if ($query) {
            $id = $query->id;

            $username = $query->sub ? $query->sub : -1;

            Session::put('user', $query->id);
            Session::save();
            return Redirect('http://' . $username . '.supportpanel.net/panel/que');
        } else {
            return Redirect('user/login')->with('wrong-email', 'ایمیل اشتباه وارد شده است');
        }
    }

    /**
     * delete admin
     */
    public function delete($id)
    {
        User::whereId($id)->delete();
        return redirect('/');
    }

    /**
     * get subdomain
     * return a name
     */
    private function getSubdomain(Request $request)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        if (count($url_array) == 3)
            $subdomain = $url_array[0];
        else $subdomain = null;

        return $subdomain;
    }

}

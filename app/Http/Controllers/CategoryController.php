<?php

namespace App\Http\Controllers;

use App\Category;
use App\Admin;
use App\Panel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('IfAdminLogged');
    }

    /**
     * @param Request $req
     * @return string
     */
    public function index(Request $req)
    {
        $sub = $this->getSubdomain($req);
        $dbSubdomain = Admin::whereSub(null)->pluck('username')->toArray();
        if (!$sub || !in_array($sub, $dbSubdomain))
            return 'لینک نامعتبر';
        $adminId = Session::get('admin');

        $admin = Admin::whereId($adminId)->first();
        $nav_color = Panel::whereSub($sub)->pluck('color');
        $categories = Category::whereSub($sub)->get();
        return view('category.index')
            ->withCategories($categories)
            ->withChatOnly($admin->chat_only)
            ->withAdminav($admin)
            ->withColor($nav_color);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $adminId = Session::get('admin');
        $admin = Admin::whereId($adminId)->first();

        $sub = $this->getSubdomain($req);
        $dbSubdomain = Admin::whereSub(null)->pluck('username')->toArray();
        if (!$sub || !in_array($sub, $dbSubdomain))
            return 'لینک نامعتبر';
        $nav_color = Panel::whereSub($sub)->pluck('color');


        return view('category.create')
            ->withChatOnly($admin->chat_only)
            ->withAdminav($admin)
            ->withColor($nav_color);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * field require validation
         */

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',

        ],
            [
                'name.required' => 'put some value in title field',
                'description.required' => 'put some value in description field',
            ]);
        try{
            $input = $request->all();
            $subdomain = $this->getSubdomain($request);
            $cat = Category::create([
                'name' => $input['name'],
                'description' => $input['description'],
                'sub' => $subdomain
            ]);
            return redirect()->back()->with('success', 'دپارتمان با موفقیت اضافه شد');
        } catch (\Illuminate\Database\QueryException $ex) {

            return Redirect()->back()->with('catch',
                'مشکلی رخ داده است،‌لطفا دوباره امتحان کنید');
        }
    }


    public function edit($id)
    {
        $category = Category::whereId($id)->first();
        $adminId = Session::get('admin');
        $admin = Admin::whereId($adminId)->first();

        return view('category.edit')
            ->withChatOnly($admin->chat_only)
            ->withAdminav($admin)
            ->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'update.name' => 'required',
            'update.description' => 'required',

        ],
            [
                'update.name.required' => 'put some value in title field',
                'update.description.required' => 'put some value in description field',
            ]);
        $input = $request->all();
        Category::whereId($id)->update($input['update']);
        return redirect('cat');

    }

    /**
     * delete admin
     */
    public function delete($id)
    {
        Category::whereId($id)->delete();
        return redirect('cat');
    }

    /**
     * get subdomain
     * return a name
     */
    private function getSubdomain(Request $request)
    {
        $url_array = explode('.', parse_url($request->url(), PHP_URL_HOST));
        if (count($url_array) == 3)
            $subdomain = $url_array[0];
        else $subdomain = null;

        return $subdomain;
    }
}
